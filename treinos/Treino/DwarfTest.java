

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DwarfTest
{
    @Test
    public void testarCriacaoDwarf(){
    Dwarf dwarf = new Dwarf ("dwarf");
    Elfo elfo = new Elfo ("elfo");
    assertEquals(110.0,dwarf.getVida(),1e-9);
    
    }
    @Test
    
    public void DwarfTomaDano(){
     Dwarf dwarf = new Dwarf ("dwarf");
    Elfo elfo = new Elfo ("elfo");
    elfo.atirarFlecha(dwarf);
    assertEquals(100.0,dwarf.getVida(),1e-9);
    }
}
