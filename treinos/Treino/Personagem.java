

public class Personagem
{
    protected String nome;
    protected double vida;
    protected double qtdDano;
    protected Status status;
    protected int experiencia;
    protected int experienciaPorAtk;
    
    {
        status = Status.MORTO;
        experiencia = 0;
        experienciaPorAtk = 0;
        qtdDano = 0.0;    
    }
    
    protected Personagem(String nome){
    this.nome = nome;
    }
    
    
    protected String getNome(){
    return this.nome;
    }
    
    protected void setNome(String nome){
    this.nome = nome;
    }
    
    protected double getVida(){
    return this.vida;
    }
    
    protected Status getStatus(){
    return this.status;
    }
    
    protected void aumentarXp(){
    this.experiencia = this.experiencia + this.experienciaPorAtk;
    }
    
    protected boolean podePerderVida(){
    return this.vida > 0.0;
    }
    
    protected double calcularDano(){
    return this.qtdDano;
    }
    
    protected int getExperiencia(){
    return this.experiencia;
    }
    
    protected void diminuirVida(){
        this.vida -= (this.vida >= qtdDano) ? this.calcularDano() : this.vida;
        if (this.vida == 0){
          this.status = Status.MORTO;
        }else{
          this.status = Status.SOFREU_DANO;
        }
    }
    
    
}
