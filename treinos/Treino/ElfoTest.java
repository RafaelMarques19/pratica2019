

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoTest
{
    @Test
   public void testarCriacaoElfo(){
   Elfo elfo = new Elfo("elfo");
   assertEquals(100.0,elfo.getVida(), 1e-9);   
   }
   
    @Test
   public void ElfoperdeFlechaAoAtirarEAumentarXp(){
   Elfo elfo = new Elfo("elfo");
   Dwarf dwarf = new Dwarf ("dwarf");
   elfo.atirarFlecha(dwarf);
   assertEquals(2,elfo.getQtdFlechas(),1e-9);
   assertEquals(1,elfo.getExperiencia());
   }
}
