
public class Elfo extends Personagem
{
    private int qtdFlechas = 3;

    public Elfo(String nome){
        super(nome);
        this.experienciaPorAtk = 1;
        this.vida = 100.0;
    }

    public int getQtdFlechas(){
        return qtdFlechas;

    }
    public void setQtdFlechas(int qtdFlechas){
       this.qtdFlechas = qtdFlechas;
    }

    public boolean podeAtirarFlechas(){
        return getQtdFlechas() > 0;  
    }

    public void atirarFlecha(Dwarf dwarf){
        if (podeAtirarFlechas()){
          setQtdFlechas(getQtdFlechas() -1);
          this.aumentarXp();
          dwarf.diminuirVida();
        }
    }

    
}
