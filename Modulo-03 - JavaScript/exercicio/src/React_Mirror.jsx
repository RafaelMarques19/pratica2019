import React, { Component } from 'react';
import './App.css';
/* import Filhos from './exemplos/Filhos' */
//import CompA, { CompB } from './ExemploComponenteBasico';
import {Link} from 'react-router-dom'
import ListaEpisodios from './Components/ListaEpisodios';
import EpisodioPadrao from './Components/EpisodioPadrao';
import MensagemFlash from './Components/Mensagem';
import MeuInputNumero from './Components/MeuInputNumero';
import ListaDeAvaliacoes from './Components/ListaDeAvaliacoes'
import * as axios from 'axios'

class Eps extends Component{
  constructor(props) {
    super(props)
    this.listaEpisodios = new ListaEpisodios()
    this.state = {
      episodio: this.listaEpisodios.EpisodiosAleatorios,
      assistido: false,
      exibicao: false,
      mensagemTipo: true,
      obrigatorio: true,
      avaliados: false
    } 
  }
  componentDidMount(){
    axios.get("https://pokeapi.co/api/v2/pokemon/").then( response => console.log(response) )
  }

  abrirListaAvaliados() {
    this.setState({
      avaliados: true
    })
  }

  fecharListaAvaliados() {
    this.setState({
      avaliados: false
    })
  }

  sortear () {
    const episodio = this.listaEpisodios.EpisodiosAleatorios
    this.setState({
      episodio,
      assistido: this.state.assistido
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido(episodio)
    this.setState({
      episodio: episodio,
      assistido: true
    })
  }

  notaValida( nota ){
    return nota >= 1 && nota <= 5
  }

  alterarexibicao(){
    this.setState({
      exibicao: !this.state.exibicao
    })
  }

  registrarNota(event) {
    const { episodio } = this.state
    const nota = event.target.value
    if(this.notaValida(nota)){
      episodio.avaliar(nota)
      this.setState({
        mensagemTipo: true
      })
    } else {
      this.setState({
        mensagemTipo: false
      })
    }
    this.setState({
      episodio,
      exibicao: true,
    })
  }
logout(){
  localStorage.removeItem('Authorization');
}
  render() {
    const { episodio, exibicao, assistido, mensagemTipo, obrigatorio, avaliados } = this.state
    return (
      <div >
        <div>
          <EpisodioPadrao episodio={episodio} sortearNoComp={this.sortear.bind(this)} 
           MarcarNoComp={this.marcarComoAssistido} />

          <MeuInputNumero assistido={assistido} episodio={ episodio } registrarNota={ this.registrarNota.bind(this) } 
           placeholderInput="Nota de 1 a 5" mensagemSpan="Nota:" obrigatorio={obrigatorio} />
           
           <MensagemFlash exibicao={exibicao} registrarNota={this.registrarNota.bind(this)} assistido={assistido} episodio={episodio} mensagemTipo={mensagemTipo} alterarexibicao={this.alterarexibicao.bind(this)}/>


           <ListaDeAvaliacoes lista= {this.listaEpisodios.todos} avaliados={avaliados}/>

           <button onClick={ this.abrirListaAvaliados.bind( this )}>mostrar avaliações</button>


          <Link className="a" to="/Pagina_Principal"> Voltar para Mirror</Link>
          <button type="button" onClick={this.logout.bind(this)}>deslogar</button>
        </div>
      </div>
    );
  }
}
export default Eps;