import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import './App.css';
import ListaSeries from './models/ListaSeries'
/* import RenderizaGeneros from './Components/RenderizaGeneros'
import Titles from './Components/Titles'
import InputSearch from  './Components/InputSearch'
import RenderizarInvalidas from './Components/RenderInvalida'
import RenderAtor from './Components/RenderAtor'
import RenderizarAno from './Components/RenderizaAno'
import RenderizarCreditos from './Components/RenderizarCreditos' */

class InfoJsFlix extends Component {
  state = { series: '' }

  limpar(){
    this.setState({ series: '' })
  }

  seriesInvalidas = () => {
    this.setState({ series: ListaSeries.invalidas() })
  }

  filtrarAnoEstreia = (event) => {
    this.setState({ series: ListaSeries.filtrarPorAno(parseInt(event.target.value)) })
  }

  buscarNome = (event) => {
    let retorno = ListaSeries.procurarPorNome(event.target.value)
    if(retorno == true){
      this.setState({ series: `Este nome está presente em um dos elencos.` })
    }else{
      this.setState({ series: `Este nome não faz parte de elenco algum.` })
    }
  }
  
  mediaEpisodios = () => {
    this.setState({ series: ListaSeries.mediaDeEpisodios() })
  }

  valorGasto = (event) => {
    this.setState({ series: ListaSeries.totalSalarios(parseInt(event.target.value)) })
  }

  seriePorGenero = (event) => {
    this.setState({ series: ListaSeries.queroGenero(event.target.value) })
  }

  seriePorPalavraChave = (event) => {
    this.setState({ series: ListaSeries.queroTitulo(event.target.value) })
  }

  serieCreditos = (event) => {
    this.setState({ series: ListaSeries.creditos(parseInt(event.target.value)) })
  }
  Hashtag = () => {
    let Hash = Array.prototype.hashtagSecreta()
    console.log(Hash)
  }

  render() {
    return (
      <div className="App">
        
          <Link className="a" to="/Pagina_Principal">teste Mirror</Link>
        <button className="btn" onClick={ this.seriesInvalidas.bind(this) }>Inválidas</button><button className="btn" onClick={ this.limpar.bind(this) }>Limpar</button><br/>
        <label>Filtrar séries por ano:</label> <input type="text" placeholder="Digite um ano" onBlur={ this.filtrarAnoEstreia.bind(this) }/><br/><br/>
        <label>Procurar por nome:</label> <input type="text" placeholder="Digite um ano" onBlur={ this.buscarNome.bind(this) }/><br/>
        <button className="btn" onClick={ this.mediaEpisodios.bind(this) }>Média Episódios</button><br/>
        <label>Digite o índice da série:</label> <input type="text" placeholder="Digite um índice" onBlur={ this.valorGasto.bind(this) }/><br/><br/>
        <label>Digite o gênero da série:</label> <input type="text" placeholder="Digite um gênero" onBlur={ this.seriePorGenero.bind(this) }/><br/><br/>
        <label>Digite o palavras chave para buscar uma série:</label> <input type="text" placeholder="Digite uma palavra chave" onBlur={ this.seriePorPalavraChave.bind(this) }/><br/><br/>
        <label>Digite o índice da série que deseja ver os créditos:</label> <input type="text" placeholder="Digite um índice" onBlur={ this.serieCreditos.bind(this) }/><br/><br/>
        <h2>{ this.state.series }</h2>
      </div>
    );
  }
}
export default InfoJsFlix;
