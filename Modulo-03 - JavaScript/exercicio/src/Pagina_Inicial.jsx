import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import './App.css';
import ListaSeries from './models/ListaSeries'
import ListaDeAvaliacoes from './Components/ListaDeAvaliacoes'
/* import RenderizaGeneros from './Components/RenderizaGeneros'
import Titles from './Components/Titles'
import InputSearch from  './Components/InputSearch'
import RenderizarInvalidas from './Components/RenderInvalida'
import RenderAtor from './Components/RenderAtor'
import RenderizarAno from './Components/RenderizaAno'
import RenderizarCreditos from './Components/RenderizarCreditos' */

class Mirror extends Component {
  state = { series: '' }

  limpar(){
    this.setState({ series: '' })
  }

  seriesInvalidas = () => {
    this.setState({ series: ListaSeries.invalidas() })
  }

  filtrarAnoEstreia = (event) => {
    this.setState({ series: ListaSeries.filtrarPorAno(parseInt(event.target.value)) })
  }

  buscarNome = (event) => {
    let retorno = ListaSeries.procurarPorNome(event.target.value)
    if(retorno == true){
      this.setState({ series: `Este nome está presente em um dos elencos.` })
    }else{
      this.setState({ series: `Este nome não faz parte de elenco algum.` })
    }
  }
  
  mediaEpisodios = () => {
    this.setState({ series: ListaSeries.mediaDeEpisodios() })
  }

  valorGasto = (event) => {
    this.setState({ series: ListaSeries.totalSalarios(parseInt(event.target.value)) })
  }

  seriePorGenero = (event) => {
    this.setState({ series: ListaSeries.queroGenero(event.target.value) })
  }

  seriePorPalavraChave = (event) => {
    this.setState({ series: ListaSeries.queroTitulo(event.target.value) })
  }

  serieCreditos = (event) => {
    this.setState({ series: ListaSeries.creditos(parseInt(event.target.value)) })
  }
  Hashtag = () => {
    let Hash = Array.prototype.hashtagSecreta()
    console.log(Hash)
  }

  render() {
    return (
      <div className="App">
          <Link className="a" to="/Series">Ir para as séries</Link>    
          <Link className="a" to="/Episodios"> ir para os episodios</Link>
          <Link className="a" to="/avaliacoes">ir para avaliacoes</Link>
      </div>
    );
  }
}
export default Mirror;
