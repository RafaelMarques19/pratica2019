import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import InfoJsFlix from './InfoJsFlix';
import Pagina_Inicial from './Pagina_Inicial';
import login from "./Components/login"
import React_Mirror from './React_Mirror';
import { PrivateRoute } from './Components/PrivateRoute';
import ListaDeAvaliações from './Components/ListaDeAvaliacoes'

export default class App extends Component {
  render() {
    return (
      <div className="App">
         <Router>
            <React.Fragment>
              <section>
                <PrivateRoute path="/" exact component={ Pagina_Inicial } />
                <Route path="/Series" component={ InfoJsFlix } />
                <Route path="/login" component={login}/>
                <Route path="/Pagina_Principal" component={ Pagina_Inicial }/>
                <Route path="/Episodios" component={ React_Mirror }/>
                <Route path="/avaliacoes" component={ ListaDeAvaliações}/>
              </section>
            </React.Fragment>
         </Router>
      </div>  
    );
  }
}
 


