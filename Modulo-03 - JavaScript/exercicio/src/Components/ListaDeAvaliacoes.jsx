import React, { Component } from 'react';


export default class ListaDeAvaliacoes extends Component {
    constructor( props ) {
        super( props )
        this.props = props
    }

    get getListaAvaliados() {
        const { lista } = this.props
        return lista.filter(lista => lista.nota > 0)
    }

    get getListaAvaliadosOrdenado() {
        const { lista } = this.props
        const listaAvaliados =  lista.filter(lista => lista.nota > 0)
        return listaAvaliados.sort( (a,b) => {
            if (a.temporada < b.temporada) {
                return -1;
            }
            if (a.temporada > b.temporada) {
                return 1;
            }
            // a deve ser igual a b
            return this.sortEp(a.ordemEpisodio,b.ordemEpisodio)
        })
    }

    sortEp(a,b) {
        if(a < b) {
            return -1;
        }
        if (a > b) {
            return 1;
        }
        return 0;
    }

    render() {
        const { avaliados } = this.props
        const lista = this.getListaAvaliadosOrdenado
        return avaliados ? (
            lista.map(element => {
                return (
                   <React.Fragment>
                       <p>{element.nome}</p>
                   </React.Fragment> 
                )
            })
        )  :    ''
    }
}



/* import React, {Component} from 'react'
import ListaEpisodios from './ListaEpisodios'
import Episodios from './Episodios'

 class ListaDeAvaliacoes extends Component{
    constructor(props){
        super(props)
        let listaAvaliados = new ListaEpisodios() 
    }
    listar(){
        this.avaliar.map(listaAvaliados => new Episodios(lista.nome,lista.duracao,lista.temporada,lista.ordemEpisodio,lista.thumbUrl, lista.qtdvezesAssistido, lista.nota) )        
    }
    render(){
        return(
            <div>
                <h4>{this.listar()}</h4>
            </div>
        )
    }
}
export default ListaDeAvaliacoes; */