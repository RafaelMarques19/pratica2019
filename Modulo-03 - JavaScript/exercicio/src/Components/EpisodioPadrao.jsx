import React from 'react'
  
const EpisodioPadrao = props =>{
    const {episodio} = props
    return(
        <React.Fragment>
          <div>
            <button className="button" onClick= { () => props.sortearNoComp() /*.bind( this )*/}>Proximo </button>
            <button className="button" onClick= { () => props.MarcarNoComp() }>Já Assisti </button>
          </div>
          <h2>{ episodio.nome}</h2>
          <img src= { episodio.thumbUrl } alt={ episodio.nome }></img>
          <h4> duração {episodio.duracaoEmMin}</h4>
          <h4> Temporada/Episódio {episodio.temporadaEpisodio}</h4>
          <h4> Já Assisti? {episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es)</h4>
          <h4>{(episodio.nota || "sem nota") }</h4>
        </React.Fragment>
    )   
 }

export default EpisodioPadrao