export default class Episodios{
    constructor(nome, duracao, temporada, ordemEpisodio, thumbUrl, qtdVezesAssistido, nota){
        this.nome = nome
        this.duracao = duracao
        this.temporada = temporada
        this.ordemEpisodio = ordemEpisodio
        this.thumbUrl = thumbUrl
        this.qtdVezesAssistido = this.qtdVezesAssistido || 0
        this.nota = nota;
    }

    validarNota( nota){
        nota = parseInt(nota )
        return 1 <= nota && nota <=5
    }
    avaliar( nota){
        this.nota = parseInt( nota );
    }
    get duracaoEmMin(){
        return `${ this.duracao} min`
    }

    get temporadaEpisodio(){
        return `${ this.temporada.toString().padStart(2, '0')}/${ this.ordemEpisodio.toString().padStart(2, '0')}`
    }    
}