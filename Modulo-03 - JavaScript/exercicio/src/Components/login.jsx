import React, {Component} from 'react'
import * as axios from 'axios'

export default class Login extends Component{
    constructor(props){
        super(props)
        this.state = {
            email: '',
            password: ''
        }
        this.trocaValoresState = this.trocaValoresState.bind(this)
    }
    trocaValoresState(evt){
        const {name,value} = evt.target;
        this.setState({
            [name]: value
        })
    }

    logar(evt) {
        evt.preventDefault();
        const {email, password} = this.state
        if(email && password){
            //executo a regra de login
            axios.post('http://localhost:1336/login',{
                email: this.state.email,
                password: this.state.password
            }).then( resp =>{
                localStorage.setItem('Authorization', resp.data.token);
                this.props.history.push("/");
                }
            )
        }
    }

    render(){
        return(
            <React.Fragment>
                <h5>logar</h5>
                <input type="text" name="email" id="email" placeholder="digite o email" onChange={this.trocaValoresState}></input>
                <input type="password" name="password" id="password" placeholder="digite o password" onChange={this.trocaValoresState}></input>
                <button type="button" onClick={this.logar.bind( this)} >Logar</button>
            </React.Fragment>
        )
    }
}