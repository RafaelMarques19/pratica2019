import React from 'react';

const Titles = props => {
    return (
        <React.Fragment>
            <div className="container">
                <div className="poster">
                    { props.listaSeries.map((serie) => { 
                        return (
                        <React.Fragment>
                            <div className='title-item'>
                                <div className={serie.styleSetup}>
                                    <p>{serie.titulo}</p>
                                    <p>{serie.elenco}</p> 
                                    <p>{serie.numeroEpisodios}</p>
                                    <p>{serie.diretor}</p>
                                </div>
                            </div>
                        </React.Fragment> )
                    }) }
                    
                </div>
            </div>
        </React.Fragment>
    )
} 
export default Titles;