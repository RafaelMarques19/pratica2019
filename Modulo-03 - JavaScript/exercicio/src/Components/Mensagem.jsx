import React from 'react';
import "../App2.css"

 const MensagemFlash = (props) => {
    const { exibicao, mensagemTipo } = props
    const mensagemEnviada = <span id="sucesso">Nota registrada com sucesso!</span>
    const mensagemFalha = <span id="falha">Informar uma nota válida (entre 1 e 5)</span>
    
    function setTempo( valor ){
        setTimeout(() => {
            props.alterarexibicao()
          }, valor || 3000)
    }

    function ChecaTipo() {
        return ( mensagemTipo ? mensagemEnviada : mensagemFalha)
    }

    return (
        <div>
            <h5>{ exibicao  ? ChecaTipo() : '' } { exibicao ? setTempo(2000) : ''}</h5>
        </div>
    )
}
export default MensagemFlash;
