import React from 'react';

const MeuInputNumero = props => {
    const { assistido, episodio, obrigatorio } = props

    const campoNota = ( corInput = "blue", obrigatorio ) => {
        const corAplicar = corInput
        const estilo = `1px solid ${corAplicar}`
        return (
            episodio.assistido && (
                <div >
                    {typeof props.mensagemSpan !== undefined ? <span>{props.mensagemSpan}</span> : ''}
                    <input  type="number" style={{ border: estilo }} placeholder={props.placeholderInput} onBlur={props.registrarNota} />
                    { obrigatorio ? <span>*obrigatório*</span> : ''}
                </div>
        )
    )
}

    function gerarCampoNota(obrigatorio) {
        return (
        <div>       
            { obrigatorio ? campoNota( "red", obrigatorio ) : campoNota(obrigatorio)}
        </div>
    )
}

    return (
        <React.Fragment>
            <div>
                {assistido ? gerarCampoNota(obrigatorio) : ''}
            </div>
        </React.Fragment>
    )
}
export default MeuInputNumero;