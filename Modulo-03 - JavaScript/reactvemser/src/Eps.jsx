import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ListaTime from './exemplos/ListaTime'
/* import Filhos from './exemplos/Filhos' */
//import CompA, { CompB } from './ExemploComponenteBasico';
import Familia from './exemplos/Familia';
import ListaEpisodios from './models/ListaEpisodios';
import EpisodioPadrao from './components/EpisodioPadrao';
import TesteRenderizacao from './components/TesteRenderizacao'

console.log(ListaTime)

class Eps extends Component{
  constructor( props ){
    super( props )
    this.listaEpisodios = new ListaEpisodios();
   // this.sortear = this.sortear.bind( this )
    this.state = {
      episodio : this.listaEpisodios.EpisodiosAleatorios,
      exibirMensagem: false
    }
  }

   sortear = () =>{
    const episodio = this.listaEpisodios.EpisodiosAleatorios
    this.setState({
      episodio
    }) 
  }

  marcarComoAssistido = () =>{
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido (episodio)
    this.setState({
      episodio
    })
  }
  
  avaliarEpisodio(){
    const { episodio } = this.state
    this.listaEpisodios.avaliarEpisodio(episodio)
    this.setState({
      episodio
    })
  }

   registrarNota(evt){
    const {episodio} = this.state
     episodio.avaliar(evt.target.value)
     this.setState({
      episodio,
      exibirMensagem : true
     })
     setTimeout(() => {
       this.setState({
         exibirMensagem: false
       })
     }, 5000)
   
  } 

  gerarCampoNota(){
    return(
      <div>
      {
        this.state.episodio.assistido && (
          <div>
            <span> Qual sua nota para este episodio? </span>
            <input id="notaIn" type="number" placeholder = "1 a 5" onBlur ={this.registrarNota.bind(this)}></input>
          </div>
        )
      }
      </div>
    )
  }
  render(){
    const { episodio, exibirMensagem } = this.state
    return (
      <div className="App">
        <div className="App-Header">
           <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear.bind(this) } MarcarNoComp={ this.marcarComoAssistido.bind(this) }/>
          {this.gerarCampoNota()}
          <h5>{exibirMensagem ? 'Nota registrada com sucesso' : ''}</h5>   
         
        </div>
      </div>
    );
  }
 
}

export default Eps;
