const pokeApi1 = new PokeApi();
const pokeinput = document.getElementById( 'idpoke' )
let igual = 0
const array = []
function renderizacaoPokemon( pokemon ) {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );
  const nome = dadosPokemon.querySelector( '.nome' );
  const id = dadosPokemon.querySelector( '.id' );
  const peso = dadosPokemon.querySelector( '.peso' );
  const tipo = dadosPokemon.querySelector( '.tipo' );
  const altura = dadosPokemon.querySelector( '.altura' );
  const status = dadosPokemon.querySelector( '.status' );
  const imagens = dadosPokemon.querySelector( '.foto' );
  const tipos = dadosPokemon.querySelector( '.tipos' );
  const dados = dadosPokemon.querySelector( '.stat' );
  imagens.src = pokemon.imagem;
  while ( status.hasChildNodes() ) {
    status.removeChild( status.firstChild );
  }
  while ( tipo.hasChildNodes() ) {
    tipo.removeChild( tipo.firstChild );
  }
  nome.innerHTML = `nome: ${ pokemon.nome }`;
  id.innerHTML = `id: ${ pokemon.id }`;
  peso.innerHTML = `peso: ${ pokemon.peso } Kg`;
  altura.innerHTML = `Altura : ${ pokemon.altura } cm`;
  tipos.innerHTML = 'Tipos';
  dados.innerHTML = 'Status';
  pokemon.tipo.forEach( tiposp => {
    const li = document.createElement( 'li' );
    const p = document.createElement( 'p' );
    p.textContent = `${ tiposp.type.name } |`;
    li.appendChild( p );
    tipo.appendChild( li );
  } );
  pokemon.status.forEach( stat => {
    const list = document.createElement( 'li' );
    const listp = document.createElement( 'p' );
    listp.textContent = `${ stat.stat.name } : ${ stat.base_stat }|`;
    list.appendChild( listp );
    status.appendChild( list );
  } );
}
// função para gerar um pokemon aleatório
function Random() { // eslint-disable-line no-unused-vars
  const min = Math.ceil( 1 );
  const max = Math.floor( 802 );
  const numeroSorteado = Math.floor( Math.random() * ( max - min ) ) + min;
  // se o array ja possuir o numero sorteado ele não deixa a pesquisa
  if ( array.includes( numeroSorteado ) ) {
    alert( `pokemon ja buscado aleatoriamente, o valor pesquisado é ${ numeroSorteado }` ) // eslint-disable-line no-alert
  } else {
    const pokemonEspecifico = pokeApi1.buscarEspecifico( numeroSorteado );
    pokemonEspecifico.then( pokemon => {
      const poke = new Pokemon( pokemon );
      renderizacaoPokemon( poke )
      // o array recebe o valor do numero sorteado
      array.push( numeroSorteado )
    } );
  }
}
// função para renderizar sempre o primeiro pokemon (Bulbassauro)
function renderizar1() {
  const pokemonEspecifico = pokeApi1.buscarEspecifico( 1 );
  pokemonEspecifico.then( pokemon => {
    const poke = new Pokemon( pokemon );
    renderizacaoPokemon( poke )
    igual = pokeinput.value;
  } );
}
// realiza as validações para realizar a renderização
function validarPorId( idPoke ) {
  // se o campo estiver zerado ou o valor a ser pesquisado  for menor que 1 ou maior que 802
  if ( ( idPoke.length <= 0 ) || ( pokeinput.value < 1 ) || ( pokeinput.value > 802 ) ) {
    alert( 'favor preencher os com um id entre  1 e 807' ); // eslint-disable-line no-alert
  }
  // se a variavel igual tiver o mesmo valor do input
  if ( igual === pokeinput.value ) {
    alert( ' esta pesquisando pelo mesmo pokemon' ); // eslint-disable-line no-alert
  } else {
    const pokemonEspecifico = pokeApi1.buscarEspecifico( pokeinput.value );
    pokemonEspecifico.then( pokemon => {
      const poke = new Pokemon( pokemon );
      renderizacaoPokemon( poke )
      // a variavel igual sempre recebera o valor do input
      igual = pokeinput.value;
    } );
  }
}
// adiciona os eventos
pokeinput.addEventListener( 'blur', () => validarPorId( pokeinput.value ) );

