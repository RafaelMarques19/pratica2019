class Pokemon {// eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.nome = obj.name;
    this.id = obj.id;
    this.tipo = obj.types;
    this.altura = obj.height * 10;
    this.peso = obj.weight / 10;
    this.status = obj.stats;
    this.imagem = obj.sprites.front_default;
  }
}
