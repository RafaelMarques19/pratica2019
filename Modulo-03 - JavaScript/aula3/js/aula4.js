/* class Turma{
    constructor(ano){
        this.ano = ano;
    }
    apresentarAno(){
        console.log(`Essa turma é do ano de:  ${this.ano}`);
    }
    static info(){
        console.log(`Testando informações`);
    }
    get anoTurma(){
        console.log(this.ano);
    }
    set localMetodo(localizacao){
        this.localTurma = localizacao;
    }
}

/* const vemser = new Turma("2019/02");
vemser.apresentarAno();
vemser.anoTurma;
vemser.local = "DBC";
console.log(vemser.localTurma); */

/* class Vemser extends Turma {
    constructor(ano,local,qtdAlunos){
            super(ano)
            this.local = local;
            this.qtdAlunos = qtdAlunos;

    }
    descricao(){
        console.log(`Turma do Vem Ser ano ${this.ano} realizado na ${this.local}, quantidade de alunos ${this.qtdAlunos}`);
    }
}

const vemser = new Vemser("2019/02","DBC",17);
vemser.descricao(); */ 

/* let defer = new Promise((resolve,reject)=> {
    setTimeout(()=>{

    
    if (true){
        resolve('foi resolvido');
    }else{
        reject('erro');
    }
},3000);
});

defer
    .then((data)=>{
        console.log(data);
        return "novo resultado";
    })//resolvido
    .then((data)=>{
        console.log(data);
    })
    .catch((erro)=> console.log(erro));//trata o erro */


    //pending
    //resolved
    //rejected
   // let pokemon = fetch(`https://pokeapi.co/api/v2/pokemon/`);
   

    
    //pokemon
        
      //  .then(data =>data.json())
        //.then(data=>{console.log(data.results)});


        const valor1 = new Promise((resolve, reject) => {
            setTimeout(() =>{
                resolve({valorAtual: '1'})
            },4000);
        });


        const valor2 = new Promise((resolve, reject) => {
            resolve({valorAtual: '2'})
        });

       /*  Promise 
            .all([valor1,valor2])
            .then(resposta =>{
                console.log(resposta);
            }) */

            Promise 
            .race([valor1,valor2])
            .then(resposta =>{
                console.log(resposta);
            })