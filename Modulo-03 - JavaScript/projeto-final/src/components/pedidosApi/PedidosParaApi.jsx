import * as axios from 'axios';

export default class PedidosParaApi {
    constructor( ){
        this.obj = []
        this.baseUrl = 'http://localhost:1337'
    }

    pedidoApi(url,id){
        url += id ? `${id-1}` : ''
        axios.get( `${this.baseUrl}/${url}`,{
            headers: {
                authorization: localStorage.getItem('Authorization')
            }
        })
        .then( resp => {
            this.obj = resp.data
            console.log(this.obj);
        }).catch(function (error){
            console.log(error);
        })
    }

    pedidoListaAgencias(){
        this.pedidoApi('agencias')
    }

    pedidoListaClientes(){
        this.pedidoApi('clientes');
    }

    pedidoTiposContas(){
        this.pedidoApi('tipoContas');
    }

    pedidoListaContasDeClientes(){
        this.pedidoApi('conta/clientes');
    }

    pedidoAgencia( id ) {
        this.pedidoApi( 'agencia/', id )
    }

    pedidoCliente(id){
        this.pedidoApi('cliente/',id);
    }

    pedidoTipoConta( id ) {
        this.pedidoApi( 'tiposConta/', id )
    }

    pedidoContaCliente( id ) {
        this.pedidoApi( 'conta/cliente/', id )
    }

    get pedidoDetalhesContaCliente() {
        return this.obj.conta;
    }

    get pedidoDetalhesCliente(){
        return this.obj.cliente;
    }

    get pedidoDetalheTipoConta() {
        return this.obj.tipos;
    }
    
    get pedidoDetalhesAgencia() {
        return this.obj.agencias;
    }

    get pedidoDeAgencias(){
        console.log(this.obj.agencias);
        return this.obj.agencias
    }

    get pedidoDeClientes(){
        console.log(this.obj.clientes)
        return this.obj.clientes
    }

    get pedidoDeTiposDeContas(){
        console.log(this.obj.tipos);
        return this.obj.tipos;
    }

    get pedidoDeTodasContasDeClientes(){
        console.log(this.obj.agencias);
        return this.obj.cliente_x_conta;
    }

}