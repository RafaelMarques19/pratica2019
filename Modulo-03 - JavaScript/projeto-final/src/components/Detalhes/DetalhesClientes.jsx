import React, { Component } from 'react';
import './Detalhes.css'


export default class CardDetalhes extends Component {
    constructor( props ) {
        super( props )
        this.props = props

    }
    render() {
        const { obj } = this.props
        return (
            <div className="containerDC">
                {
                    Object.keys(obj).map(key => {
                        return typeof obj[key] === 'object' ? 
                            Object.keys(obj[key]).map(key2 => {
                                return typeof obj[key][key2] === 'object' ?
                                    Object.keys(obj[key][key2]).map(key3 => {
                                        return <p>{ `${key3.toUpperCase()}: ${obj[key][key2][key3]}` }</p> 
                                    })
                                    :
                                    <p>{ `${key2.toUpperCase()}: ${obj[key][key2]}` }</p> 
                                
                            })
                            :
                            <p>{ `${key.toUpperCase()}: ${obj[key]}` }</p> 
                    })
                }
            </div>
        )
    }
}
