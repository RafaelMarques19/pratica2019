import React, {Component} from 'react'
import './Cabecalho.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

export default class Cabecalho extends Component{
    constructor(props){
        super(props)
        this.props = props;

        
    }
    logout() {
        localStorage.removeItem('Authorization');
        localStorage.removeItem('Username');
        sessionStorage.clear();
    }
    render(){
        return(
            <div className ="nav">
                <h2 className> {this.props.nomeC}</h2>
                    <div className="divisor">
                        <Link to="/">Home</Link>
                    </div> 
                    <div className="divisor">
                        <Link to="/agencias">agencias</Link>
                    </div> 
                    <div className="divisor">
                        <Link to="/clientes">Clientes</Link>
                    </div> 
                    <div className="divisor">
                        <Link to="/tiposContas">Tipos De Contas</Link>
                    </div> 
                    <div className="divisor">
                        <Link to="/conta/clientes">Contas De Clientes</Link>
                    </div>
                    <div className="divisor" >
                        <Link to="/login" onClick={this.logout.bind(this)}>Logout</Link>
                    </div> 
                {/*     <li><Link to="/agencias">agencias, </Link></li>
                    <li><Link to="/agencias">agencias, </Link></li>
                    <li><Link to="/agencias">agencias, </Link></li> */}
                
            </div>    
        )

    }
}