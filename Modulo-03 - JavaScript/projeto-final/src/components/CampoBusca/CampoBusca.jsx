import React, { Component } from 'react';
import "./campoBusca.css"


export default class InputSearch extends Component {
    constructor( props ) {
        super( props )
        this.props = props
        
    }
    
    render() {
        const { buscar } = this.props
        return (
            <React.Fragment>
                <div className="boxcampo">
                    <input type="text" className="input-filter" placeholder="Buscar..." onChange={ buscar }/>
                </div>
            </React.Fragment>
        )
    }
}