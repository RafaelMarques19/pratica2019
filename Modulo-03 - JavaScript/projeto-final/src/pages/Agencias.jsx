import React, {Component} from 'react';
import PedidosParaApi from './../components/pedidosApi/PedidosParaApi';
import Cabecalho from './../components/Cabecalho/Cabecalho'
import './../components/ContainerAgencias/Agencias.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import CampoBusca from './../components/CampoBusca/CampoBusca'


export default class Agencias extends Component{
    constructor(props){
        super(props)
        this.props = props
        this.api = new PedidosParaApi()
        this.state = {
            ListarAgencias : this.api.pedidoDeAgencias,
            is_digital: null
        }
    }
    componentWillMount(){
        this.api.pedidoListaAgencias()
         setTimeout(() => {
            this.setState({
                ListarAgencias: this.api.pedidoDeAgencias
            })
        }, 1000);//this.api.pedidoDeAgencias
    }

    buscar( evt ) {
        const agencias = this.api.pedidoDeAgencias
        const busca = agencias.filter(agencias => agencias.nome.toLowerCase().includes(evt.target.value.toLowerCase()))
        this.setState({
            ListarAgencias: busca
        })
    }

    /* filtrarAgenciasDigitais() {
        const agencias = this.api.listaTodasAgencias
        const busca = agencias.filter(ag => sessionStorage.getItem(ag.id) === 'true')
        this.setState({
            listaAgencias: busca
        })
    }
 */
buscarIsDigital() {
    const todasAgencias = this.api.pedidoDeAgencias
    const retorno = todasAgencias.filter(e => e.isDigital == true)
    this.setState({
        ListarAgencias: retorno
    })
}
marcarComoDigital(event) {
    const { ListarAgencias } = this.state
    const ag = ListarAgencias.find(e => e.id === event)
    ag.isDigital = !ag.isDigital
    console.log(ListarAgencias)
    this.setState({
        ListarAgencias
    })
}


    render(){
        const {ListarAgencias} = this.state
        this.props = this.state
        return(
            <React.Fragment>
                <Cabecalho nomeC={"Agencias"}/>   
                <div className="boxcampo">
                    <CampoBusca buscar={this.buscar.bind(this)}/>
                    <button className="button" onClick={this.buscarIsDigital.bind(this)}>filtro digital </button>
                </div>
                <div>
                </div>
            <div>
                {ListarAgencias ? ListarAgencias.map( agencias => {
                    return(
                        <React.Fragment>                   
                          <Link to={ `agencia/${agencias.id}` }>
                        <div className='containerAg' key={agencias.codigo}>
                            <h2>Agencia{agencias.id}</h2>
                            <ul>
                                <li>ID: {agencias.id}</li>
                                <li>codigo: {agencias.codigo}</li> 
                                <li>Nome: {agencias.nome}</li>      
                            </ul>                            
                        </div>
                        </Link> 
                    
                        <button className="button" onClick={() =>this.marcarComoDigital(agencias.id)}>IsDigital</button> 
                       
                        </React.Fragment>
                    )
                })
                :
                <div>
                    <h1>{" "}</h1>
                </div>
            }
            </div>
            </React.Fragment>
        )
    }
}

{/* <p key={agencias.codigo} id={agencias.id}>{agencias.codigo}{agencias.nome} {agencias.id} </p> */}