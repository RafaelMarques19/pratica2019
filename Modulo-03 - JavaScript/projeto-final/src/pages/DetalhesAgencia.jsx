import React, {Component} from 'react';
import PedidosParaApi from '../components/pedidosApi/PedidosParaApi';
import Cabecalho from '../components/Cabecalho/Cabecalho'
import DetalhesAgencias from '../components/Detalhes/DetalhesAgencias'


export default class DetalhesAgencia extends Component{

    constructor(props){
        super(props)
        this.api = new PedidosParaApi();
        this.state ={
            detalhes: null
        }
    }

    componentWillMount(){
        const{id} = this.props.match.params
        this.api.pedidoAgencia( id )
        setTimeout(() =>{
            this.setState({
                agencia: this.api.pedidoDetalhesAgencia
            })
        },1000);
    }

    render(){
        const {agencia} = this.state

        return(
            <React.Fragment>
                <Cabecalho nomeC={"Agencias"}/>      
                 {
                     agencia ? (
                         <div>
                             {
                                  <DetalhesAgencias obj={agencia}/>
                             }
                         </div>
                     )  :
                     (
                         <h2>{""}</h2>
                     )
                 }
            </React.Fragment>
        )
    }
}

