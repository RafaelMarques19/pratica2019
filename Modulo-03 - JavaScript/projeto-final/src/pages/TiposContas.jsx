import React, {Component} from 'react';
import PedidosParaApi from './../components/pedidosApi/PedidosParaApi';
import Cabecalho from './../components/Cabecalho/Cabecalho'
import "./../components/TiposDeContas/TiposDeConta.css"
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import CampoBusca from './../components/CampoBusca/CampoBusca'


export default class TiposContas extends Component{
    constructor(props){
        super(props)
        this.api = new PedidosParaApi()
        this.state = {
            ListarTipos : this.api.pedidoDeTiposDeContas
        }
    }
    componentWillMount(){
        this.api.pedidoTiposContas()
         setTimeout(() => {
            this.setState({
                ListarTipos: this.api.pedidoDeTiposDeContas
            })
        }, 1000);
    }

    buscar( evt ) {
        const tiposContas = this.api.pedidoDeTiposDeContas
        const busca = tiposContas.filter(TiposC => TiposC.nome.toLowerCase().includes(evt.target.value.toLowerCase()))
        this.setState({
            ListarTipos: busca
        })
    }

    render(){
        const {ListarTipos} = this.state
        return(
            <React.Fragment>    
                <Cabecalho nomeC={"Tipos de Contas"}/>  
                <div>
                    <CampoBusca buscar={this.buscar.bind(this)}/>
                </div>
            <div>
                {ListarTipos ? ListarTipos.map( TiposC => {
                    return(
                        <React.Fragment>
                            <Link to={`/tiposConta/${TiposC.id}`}>
                            <div className="containerTDC">
                                <ul key={TiposC.id}>
                                    <li>{TiposC.id}</li>
                                    <li>{TiposC.nome}</li>
                                </ul>
                            </div>
                            </Link>                       
                        </React.Fragment>
                    )
                })
                :
                <div>
                    <h1>{''}</h1>
                </div>
            }
            </div>
            </React.Fragment>
        )
    }
}