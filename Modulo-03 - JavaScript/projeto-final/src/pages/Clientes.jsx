import React, {Component} from 'react';
import PedidosParaApi from './../components/pedidosApi/PedidosParaApi';
import Cabecalho from './../components/Cabecalho/Cabecalho'
import './../components/ContainerClientes/ContainerClientes.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import CampoBusca from './../components/CampoBusca/CampoBusca'


export default class Clientes extends Component{
    constructor(props){
        super(props)
        this.api = new PedidosParaApi()
        this.state = {
            ListarClientela : this.api.pedidoDeClientes
        }
    }
    componentWillMount(){
        this.api.pedidoListaClientes()
         setTimeout(() => {
            this.setState({
                ListarClientela: this.api.pedidoDeClientes
            })
        }, 1000);
    }


    buscar( evt ) {
        const clientes = this.api.pedidoDeClientes
        const busca = clientes.filter(clientes => clientes.nome.toLowerCase().includes(evt.target.value.toLowerCase()))
        this.setState({
            ListarClientela: busca
        })
    }


    render(){
        const {ListarClientela} = this.state
        return(
            <React.Fragment>   
                <Cabecalho nomeC={"Clientes"}/>   
                <div>
                  <CampoBusca buscar={this.buscar.bind(this)}/>
                </div>           
                {ListarClientela ? ListarClientela.map( clientes => {
                    return(
                        <React.Fragment>
                            <Link to={ `cliente/${clientes.id}` }>
                        <div className="containerCli" key={clientes.cpf}>
                            <ul className='clienteUl'>
                                <li> ID: {clientes.id}</li>
                                <li>Nome: {clientes.nome}</li>
                                <li>CPF: {clientes.cpf}</li>         
                            </ul>
                        </div>
                        </Link>
                        </React.Fragment>
                    )
                })
                :
                <div>
                    <h1>{''}</h1>
                </div>
            }
            
            </React.Fragment>
        )
    }
}
/* {clientes.nome}{clientes.cpf} */