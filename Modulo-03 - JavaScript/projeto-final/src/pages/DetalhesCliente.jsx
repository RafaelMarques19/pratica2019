import React, {Component} from 'react';
import PedidosParaApi from '../components/pedidosApi/PedidosParaApi';
import Cabecalho from '../components/Cabecalho/Cabecalho'
import DetalhesClientes from '../components/Detalhes/DetalhesClientes'


export default class DetalhesCliente extends Component{
    constructor(props){
        super(props)
        this.api = new PedidosParaApi();
        this.state ={
            cliente: null
        }
    }

    componentWillMount(){
        const{id} = this.props.match.params
        this.api.pedidoCliente( id )
        setTimeout(() =>{
            this.setState({
                cliente: this.api.pedidoDetalhesCliente
            })
        },1000);
    }

    render(){
        const {cliente} = this.state

        return(
            <React.Fragment>
                <Cabecalho nomeC={"Clientes Detalhados"}/>      
                 {
                     cliente ? (
                         <div>
                             {
                                  <DetalhesClientes obj={cliente}/>
                             }
                         </div>
                     )  :
                     (
                         <h2>{""}</h2>
                     )
                 }
            </React.Fragment>
        )
    }
}



