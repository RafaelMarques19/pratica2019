import React, {Component} from 'react';
import PedidosParaApi from './../components/pedidosApi/PedidosParaApi';
import Cabecalho from '../components/Cabecalho/Cabecalho';
import './../components/ContasClientes/ContasClientes.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import CampoBusca from './../components/CampoBusca/CampoBusca'

export default class ContasClientes extends Component{
    constructor(props){
        super(props)
        this.api = new PedidosParaApi()
        this.state = {
            listarContasDeTodosClientes : this.api.pedidoDeTodasContasDeClientes
        }
    }
    componentWillMount(){
        this.api.pedidoListaContasDeClientes()
         setTimeout(() => {
            this.setState({
                listarContasDeTodosClientes: this.api.pedidoDeTodasContasDeClientes
            })
        }, 1000);
    }

    buscar( evt ) {
        const contasClientes = this.api.pedidoDeTodasContasDeClientes
        const busca = contasClientes.filter(tdClientes => tdClientes.tipo.nome.toLowerCase().includes(evt.target.value.toLowerCase()))
        this.setState({
            listarContasDeTodosClientes: busca
        })
    }

    render(){
        const {listarContasDeTodosClientes} = this.state
        return(
            <React.Fragment>      
                <Cabecalho nomeC={"Contas De Clientes"} />
                <div>
                    <CampoBusca buscar={this.buscar.bind(this)}/>
                </div>
            <div>
                {listarContasDeTodosClientes ? listarContasDeTodosClientes.map( tdClientes => {
                    return(
                        <React.Fragment>
                        <Link to={`/conta/cliente/${tdClientes.id}`}>
                            <div className="containerTd">
                                <ul>
                                    <li> ID:{tdClientes.id}</li>
                                    <li> Tipo: {tdClientes.tipo.nome}</li>
                                    <li>Nome Cliente: {tdClientes.cliente.nome}</li>
                                </ul>
                            </div>
                        </Link>    
                        </React.Fragment>
                    )
                })
                :
                <div>
                    <h1>{''}</h1>
                </div>
            }
            </div>
            </React.Fragment>
        )
    }
}