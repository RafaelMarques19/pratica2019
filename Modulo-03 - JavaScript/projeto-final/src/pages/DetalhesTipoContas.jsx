import React, {Component} from 'react';
import PedidosParaApi from '../components/pedidosApi/PedidosParaApi';
import Cabecalho from '../components/Cabecalho/Cabecalho'
import DetalhesTiposConta from '../components/Detalhes/DetalhesTiposConta'


export default class DetalhesTipoContas extends Component{

    constructor(props){
        super(props)
        this.api = new PedidosParaApi();
        this.state ={
        tipoContas: null
        }
    }

    componentWillMount(){
        const{id} = this.props.match.params
        this.api.pedidoTipoConta( id )
        setTimeout(() =>{
            this.setState({
                tipoContas: this.api.pedidoDetalheTipoConta
            })
        },1000);
    }

    render(){
        const {tipoContas} = this.state

        return(
            <React.Fragment>
                <Cabecalho nomeC={"Detalhes De Tipos De Contas"}/>      
                 {
                     tipoContas ? (
                         <div>
                             {
                                  <DetalhesTiposConta obj={tipoContas}/>
                             }
                         </div>
                     )  :
                     (
                         <h2>{""}</h2>
                     )
                 }
            </React.Fragment>
        )
    }
}