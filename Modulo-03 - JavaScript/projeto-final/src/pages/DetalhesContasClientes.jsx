import React, {Component} from 'react';
import PedidosParaApi from '../components/pedidosApi/PedidosParaApi';
import Cabecalho from '../components/Cabecalho/Cabecalho'
import DetalhesContasClientes from '../components/Detalhes/DetalhesContaClientes'


export default class DetalhesContaClientes extends Component{

    constructor(props){
        super(props)
        this.api = new PedidosParaApi();
        this.state ={
        tipoContas: null
        }
    }

    componentWillMount(){
        const{id} = this.props.match.params
        this.api.pedidoContaCliente( id ) 
        setTimeout(() =>{
            this.setState({
                tipoContas: this.api.pedidoDetalhesContaCliente
            })
        },1000);
    }

    render(){
        const {tipoContas} = this.state

        return(
            <React.Fragment>
                <Cabecalho nomeC={"Detalhes das Contas Dos Clientes"}/>      
                 {
                     tipoContas ? (
                         <div>
                             {
                                  <DetalhesContasClientes obj={tipoContas}/>
                             }
                         </div>
                     )  :
                     (
                         <h2>{""}</h2>
                     )
                 }
            </React.Fragment>
        )
    }
}