import React, { Component } from 'react';
import * as axios from 'axios';
import './login.css'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            senha: ''
        }
        this.trocaValoresState = this.trocaValoresState.bind( this )
    }

    trocaValoresState(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }

    logar(e) {
        e.preventDefault();

        const { email, senha } = this.state
        if (email && senha) {
            //executo a regra de login
            console.log('email', email);
            console.log('senha', senha);
            axios.post('http://localhost:1337/login', {
                email: this.state.email,
                senha: this.state.senha
            }).then( resp => {
                    localStorage.setItem( 'Authorization', resp.data.token );
                    localStorage.setItem( 'Username', email );
                    this.props.history.push("/")
                }
            )
        }
    }

    render() {
        return (
            <React.Fragment>
                <header className="cabecalho">
                    <h1>Seja Bem Vindo</h1>
                </header>
                <div className="boxInputs">
                    <input type="text" name="email" id="email" placeholder="Digite o email" onChange={ this.trocaValoresState } />  
                </div>
                <div className="boxInputs">
                    <input type="password" name="senha" id="senha" placeholder="Digite o password" onChange={ this.trocaValoresState }/>

                </div>
                <div className="boxInputs">
                    <button type="button" onClick={ this.logar.bind( this ) }>Logar</button> 
                </div>
                    
            </React.Fragment>
        ) 
    }
}