import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Login from './pages/Login'
import {PrivateRoute} from  './components/PrivateRoute';
import PaginaInicial from './pages/PaginaInicial'
import Agencias from './pages/Agencias'
import Clientes from './pages/Clientes'
import ContasClientes from './pages/ContasClientes'
import TiposContas from './pages/TiposContas'
import DetalhesAgencia from './pages/DetalhesAgencia'
import DetalhesCliente from'./pages/DetalhesCliente'
import DetalhesTipoContas from "./pages/DetalhesTipoContas"
import DetalhesContasClientes from './pages/DetalhesContasClientes'

import './App.css';
export default class App extends Component {
  constructor ( props ) { // eslint-disable-line no-useless-constructor
    super( props )
    this.props = props
  }
  render(){
    return (
      <Router>
        <React.Fragment>
          <Route path="/login" exact component={ Login } />
          <PrivateRoute path="/" exact component={ PaginaInicial } />
          <PrivateRoute path="/agencias" exact component={ Agencias} />
          <PrivateRoute path="/clientes" exact component={ Clientes} />
          <PrivateRoute path="/tiposContas"exact component={ TiposContas} />
          <PrivateRoute path="/conta/clientes" exact component={ ContasClientes } />
          <PrivateRoute path="/agencia/:id" exact component={ DetalhesAgencia } />
          <PrivateRoute path="/cliente/:id" exact component={ DetalhesCliente } />
          <PrivateRoute path="/tiposConta/:id" exact component={ DetalhesTipoContas } />
          <PrivateRoute path="/conta/cliente/:id" exact component={ DetalhesContasClientes } />
          
          {/* <Route path="" component={ Login } /> */}
        </React.Fragment>
      </Router>
    );

    
  }
}



