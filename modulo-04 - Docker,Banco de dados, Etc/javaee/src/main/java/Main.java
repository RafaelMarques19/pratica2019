import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main (String[] args){
        Connection conn = Connector.connect();
        try{
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'PAISES'").executeQuery();
            ResultSet es = conn.prepareStatement("select tname from tab where tname = 'ESTADOS'").executeQuery();
            ResultSet ci = conn.prepareStatement("select tname from tab where tname = 'CIDADES'").executeQuery();
            ResultSet ba = conn.prepareStatement("select tname from tab where tname = 'BAIRROS'").executeQuery();
/*
            if(!rs.next()){
                conn.prepareStatement("CREATE TABLE PAISES(\n"
                + "ID_PAIS INTEGER NOT NULL PRIMARY KEY,\n"
                + "NOME VARCHAR(100) NOT NULL\n"
                + ")\n").execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into PAISES(ID_PAIS, NOME)"
                + "VALUES(PAISES_SEQ.NEXTVAL, ?)"
            );
            pst.setString(1,"ENGLAND");
            pst.executeUpdate();

            rs = conn.prepareStatement("Select * from paises").executeQuery();
            while(rs.next()){
                System.out.println(String.format("Nome do pais: %s", rs.getString("nome")));
            }
*/

            //PARA ESTADOS 

                if(!es.next()){
                    conn.prepareStatement("CREATE TABLE ESTADOS(\n"
                            + "ID_ESTADO INTEGER DEFAULT 0 NOT NULL PRIMARY KEY,\n"
                            + "NOME VARCHAR(100) DEFAULT 'Nulo' NOT NULL)").execute();
                }
            PreparedStatement est = conn.prepareStatement("insert into ESTADOS(ID_ESTADO, NOME)"
                        + "VALUES(ESTADOS_SEQ.NEXTVAL, ?)"
                );
                est.setString(1,"Buenos Aires");
                est.executeUpdate();

                rs = conn.prepareStatement("Select * from ESTADOS").executeQuery();
                while(rs.next()){
                    System.out.println(String.format("Nome do Estado: %s", rs.getString("nome")));
                } 
                
                 // PARA CIDADES
                 
                //ResultSet ci = conn.prepareStatement("select tname from tab where tname = 'CIDADES'").executeQuery();

                if(!ci.next()){
                    conn.prepareStatement("CREATE TABLE CIDADES(\n"
                            + "ID_CIDADE INTEGER DEFAULT 0 NOT NULL PRIMARY KEY,\n"
                            + "NOME VARCHAR(100) DEFAULT 'Nulo' NOT NULL)").execute();
                }
               
            PreparedStatement cid = conn.prepareStatement("insert into CIDADES(ID_CIDADE, NOME)"
                        + "VALUES(CIDADES_SEQ.NEXTVAL, ?)"
                );
                cid.setString(1,"Buenos Aires");
                cid.executeUpdate();

                rs = conn.prepareStatement("Select * from CIDADES").executeQuery();
                while(rs.next()){
                    System.out.println(String.format("Nome da Cidade: %s", rs.getString("nome")));
                } 
                
                
                //PARA BAIRROS
              // ResultSet ba = conn.prepareStatement("select tname from tab where tname = 'BAIRROS").executeQuery();
               
               if (!ba.next()){
                   conn.prepareStatement("CREATE TABLE BAIRROS(\n"
                   + "ID_BAIRRO INTEGER DEFAULT 0 NOT NULL PRIMARY KEY,\n"
                   + "NOME VARCHAR(100) DEFAULT 'Nulo' NOT NULL)").execute();
               }
               
               PreparedStatement bai = conn.prepareStatement("insert into BAIRROS(ID_BAIRRO,NOME)"
                       + " VALUES(BAIRROS_SEQ.NEXTVAL, ?)"
               );
               
               bai.setString(1,"Caminito");
               bai.executeUpdate();
               
               rs = conn.prepareStatement("Select * from BAIRROS").executeQuery();
               while(rs.next()){
                    System.out.println(String.format("Nome do Bairro: %s", rs.getString("nome")));
               }
      
        }catch(SQLException ex){
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE,"Erro na Consulta do Main", ex);
        }
    }
}
/* CRIAR TABELAS ATÉ BAIRRO */