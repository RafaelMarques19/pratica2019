package Cartao.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import Cartoes.DTO.BandeiraDTO;
import Cartoes.Dao.BandeiraDAO;
import Cartoes.Entity.Bandeira;
import Cartoes.Entity.HibernateUtil;


public class BandeiraService {
	
	
	private static final BandeiraDAO BANDEIRA_DAO = new BandeiraDAO();
	private static final Logger LOG = Logger.getLogger(LojaService.class.getName());
	

	public void salvarBandeiras(BandeiraDTO bandeiraDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Bandeira bandeiras = BANDEIRA_DAO.parseFrom(bandeiraDTO);
		
		try {
			Bandeira bandeiraRes = BANDEIRA_DAO.buscar(0);
			if(bandeiraRes == null) {
				BANDEIRA_DAO.criar(bandeiras);
			}else {
				bandeiras.setId(bandeiraRes.getId());
				BANDEIRA_DAO.atualizar(bandeiras);
			}
			
			if(started) {
				transaction.commit();
			}
			
			bandeiraDTO.setIdBandeira(bandeiras.getId());
		}catch(Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(),e);
		}
		
	
	}


}
