package Cartao.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import Cartoes.DTO.LojaDTO;
import Cartoes.Dao.LojaDAO;
import Cartoes.Entity.HibernateUtil;
import Cartoes.Entity.Loja;

public class LojaService {
	
	private static final LojaDAO LOJA_DAO = new LojaDAO();
	private static final Logger LOG = Logger.getLogger(LojaService.class.getName());
	

	public void salvarLojas(LojaDTO lojaDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Loja lojas = LOJA_DAO.parseFrom(lojaDTO);
		
		try {
			Loja lojasRes = LOJA_DAO.buscar(0);
			if(lojasRes == null) {
				LOJA_DAO.criar(lojas);
			}else {
				lojas.setId(lojasRes.getId());
				LOJA_DAO.atualizar(lojas);
			}
			
			if(started) {
				transaction.commit();
			}
			
			lojaDTO.setIdLoja(lojas.getId());
		}catch(Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(),e);
		}
		
	
	}

}
