package Cartao.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import Cartoes.DTO.ClienteDTO;
import Cartoes.Dao.ClienteDAO;
import Cartoes.Entity.Cliente;
import Cartoes.Entity.HibernateUtil;

public class ClienteService {

	private static final ClienteDAO CLIENTE_DAO = new ClienteDAO();
	private static final Logger LOG = Logger.getLogger(LojaService.class.getName());
	

	public void salvarCliente(ClienteDTO clienteDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Cliente clientes = CLIENTE_DAO.parseFrom(clienteDTO);
		
		try {
			Cliente clienteRes = CLIENTE_DAO.buscar(0);
			if(clienteRes == null) {
				CLIENTE_DAO.criar(clientes);
			}else {
				clientes.setId(clienteRes.getId());
				CLIENTE_DAO.atualizar(clientes);
			}
			
			if(started) {
				transaction.commit();
			}
			
			clienteDTO.setIdCliente(clientes.getId());
		}catch(Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(),e);
		}
		
	
	}
}
