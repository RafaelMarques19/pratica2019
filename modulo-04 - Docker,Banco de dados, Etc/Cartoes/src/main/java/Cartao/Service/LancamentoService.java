package Cartao.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import Cartoes.DTO.LancamentoDTO;
import Cartoes.Dao.LancamentoDAO;
import Cartoes.Entity.HibernateUtil;
import Cartoes.Entity.Lancamento;


public class LancamentoService {
	private static final LancamentoDAO LANCAMENTO_DAO = new LancamentoDAO();
	private static final Logger LOG = Logger.getLogger(LancamentoService.class.getName());
	
	public void salvarLancamentos(LancamentoDTO lancamentoDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Lancamento lancamentos = LANCAMENTO_DAO.parseFrom(lancamentoDTO);
		
		try {
			Lancamento lancamentoRes = LANCAMENTO_DAO.buscar(0);
			if(lancamentoRes == null) {
				LANCAMENTO_DAO.criar(lancamentos);
			}else {
				lancamentos.setId(lancamentoRes.getId());
				LANCAMENTO_DAO.atualizar(lancamentos);
			}
			
			if(started) {
				transaction.commit();
			}
			
			lancamentoDTO.setIdLancamento(lancamentos.getId());
		}catch(Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(),e);
		}
		
	
	}

}
