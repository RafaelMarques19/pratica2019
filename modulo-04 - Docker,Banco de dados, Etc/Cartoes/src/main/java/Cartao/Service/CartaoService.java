package Cartao.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import Cartoes.DTO.CartaoDTO;
import Cartoes.Dao.CartaoDAO;
import Cartoes.Entity.Cartao;
import Cartoes.Entity.HibernateUtil;
import Cartoes.Entity.Loja;

public class CartaoService {

	
	private static final CartaoDAO CARTAO_DAO = new CartaoDAO();
	private static final Logger LOG = Logger.getLogger(LojaService.class.getName());
	

	public void salvarCartoes(CartaoDTO cartaoDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Cartao cartoes = CARTAO_DAO.parseFrom(cartaoDTO);
		
		try {
			Cartao cartaoRes = CARTAO_DAO.buscar(0);
			if(cartaoRes == null) {
				CARTAO_DAO.criar(cartaoRes);
			}else {
				cartoes.setId(cartaoRes.getId());
				CARTAO_DAO.atualizar(cartoes);
			}
			
			if(started) {
				transaction.commit();
			}
			
			cartaoDTO.setIdCartao(cartoes.getId());
		}catch(Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(),e);
		}
		
	
	}

}
