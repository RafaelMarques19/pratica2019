package Cartao.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import Cartoes.DTO.EmissorDTO;
import Cartoes.Dao.EmissorDAO;
import Cartoes.Entity.Emissor;
import Cartoes.Entity.HibernateUtil;

public class EmissorService {
	private static final EmissorDAO EMISSOR_DAO = new EmissorDAO();
	private static final Logger LOG = Logger.getLogger(LojaService.class.getName());
	

	public void salvarCliente(EmissorDTO emissorDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Emissor emissores = EMISSOR_DAO.parseFrom(emissorDTO);
		
		try {
			Emissor emissorRes = EMISSOR_DAO.buscar(0);
			if(emissorRes == null) {
				EMISSOR_DAO.criar(emissores);
			}else {
				emissores.setId(emissorRes.getId());
				EMISSOR_DAO.atualizar(emissores);
			}
			
			if(started) {
				transaction.commit();
			}else {
				
			}
			
			emissorDTO.setIdEmissor(emissores.getId());
		}catch(Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(),e);
		}			
	}
}

