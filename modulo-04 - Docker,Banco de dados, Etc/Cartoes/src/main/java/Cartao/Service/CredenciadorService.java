package Cartao.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import Cartoes.DTO.CredenciadorDTO;
import Cartoes.Dao.CredenciadoDAO;
import Cartoes.Entity.Credenciador;
import Cartoes.Entity.HibernateUtil;

public class CredenciadorService {
	
	
	private static final CredenciadoDAO CREDENCIADOR_DAO = new CredenciadoDAO();
	private static final Logger LOG = Logger.getLogger(CredenciadorService.class.getName());
	

	public void salvarCliente(CredenciadorDTO credenciadorDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Credenciador credenciadores = CREDENCIADOR_DAO.parseFrom(credenciadorDTO);
		
		try {
			Credenciador credenciadorRes = CREDENCIADOR_DAO.buscar(0);
			if(credenciadorRes == null) {
				CREDENCIADOR_DAO.criar(credenciadores);
			}else {
				credenciadores.setId(credenciadorRes.getId());
				CREDENCIADOR_DAO.atualizar(credenciadores);
			}
			
			if(started) {
				transaction.commit();
			}else {
				
			}
			
			credenciadorDTO.setIdCredenciador(credenciadores.getId());
		}catch(Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(),e);
		}			
	}

}
