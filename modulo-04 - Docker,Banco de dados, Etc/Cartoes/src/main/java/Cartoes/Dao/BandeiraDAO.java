package Cartoes.Dao;

import Cartoes.DTO.BandeiraDTO;
import Cartoes.Entity.Bandeira;

public class BandeiraDAO extends AbstractDAO<Bandeira> {

	public Bandeira parseFrom(BandeiraDTO dto) {
		Bandeira bandeiras = new Bandeira();
		if(dto.getIdBandeira() != null) {
			bandeiras = buscar(dto.getIdBandeira());
		}
		bandeiras.setNome(dto.getNome());
		bandeiras.setTaxa(dto.getTaxa());
		
		
		return bandeiras;
	}

	@Override
	protected Class<Bandeira> getEntityClass() {
		// TODO Auto-generated method stub
		return Bandeira.class;
	}

}
