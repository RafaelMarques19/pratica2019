package Cartoes.Dao;

import Cartoes.DTO.CredenciadorDTO;
import Cartoes.Entity.Credenciador;


public class CredenciadoDAO extends AbstractDAO<Credenciador> {
	
	public Credenciador parseFrom(CredenciadorDTO dto) {
		Credenciador credenciadores = new Credenciador();
		if(dto.getIdCredenciador() != null) {
			credenciadores = buscar(dto.getIdCredenciador());
		}else {
			
		}
		credenciadores.setNome(dto.getNome());	
		return credenciadores;		
	}

	@Override
	protected Class<Credenciador> getEntityClass() {
		// TODO Auto-generated method stub
		return Credenciador.class;
	}

}
