package Cartoes.Dao;

import Cartoes.DTO.CartaoDTO;
import Cartoes.Entity.Cartao;

public class CartaoDAO extends AbstractDAO<Cartao> {
	private static final ClienteDAO CLIENTE_DAO = new ClienteDAO();
	private static final EmissorDAO EMISSOR_DAO = new EmissorDAO();
	private static final BandeiraDAO BANDEIRA_DAO = new BandeiraDAO();
	
	
	public Cartao parseFrom(CartaoDTO dto) {
		Cartao cartoes = new Cartao();
		if(dto.getIdCartao() != null) {
			cartoes = buscar(dto.getIdCartao());
		}
		cartoes.setVencimento(dto.getVencimento());
		cartoes.setChip(dto.getChip());
		cartoes.setBandeiras(BANDEIRA_DAO.parseFrom(dto.getBandeiras()));
		cartoes.setClientes(CLIENTE_DAO.parseFrom(dto.getClientes()));
		cartoes.setEmissores(EMISSOR_DAO.parseFrom(dto.getEmissores()));
	
		
		return cartoes;
	}
	
	@Override
	protected Class<Cartao> getEntityClass() {
		// TODO Auto-generated method stub
		return Cartao.class;
	}

}
