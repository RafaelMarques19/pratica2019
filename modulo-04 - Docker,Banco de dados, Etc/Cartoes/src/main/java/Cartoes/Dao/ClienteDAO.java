package Cartoes.Dao;

import Cartoes.DTO.ClienteDTO;
import Cartoes.Entity.Cliente;

public class ClienteDAO extends AbstractDAO<Cliente> {
	
	public Cliente parseFrom(ClienteDTO dto) {
		Cliente clientes = new Cliente();
		if (dto.getIdCliente() != null) {
			clientes = buscar(dto.getIdCliente());
		}else {
			
		}
		clientes.setNome(dto.getNome());
		return clientes;
	}

	@Override
	protected Class<Cliente> getEntityClass() {
		// TODO Auto-generated method stub
		return Cliente.class;
	}

}
