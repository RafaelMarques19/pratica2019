package Cartoes.Dao;

import Cartoes.DTO.EmissorDTO;
import Cartoes.Entity.Emissor;

public class EmissorDAO extends AbstractDAO<Emissor> {

	public Emissor parseFrom(EmissorDTO dto) {
		Emissor emissores = new Emissor();
		if(dto.getIdEmissor() != null) {
			emissores = buscar(dto.getIdEmissor());
		}else {
			
		}
		emissores.setNome(dto.getNome());
		emissores.setTaxa(dto.getTaxa());
		
		return emissores;
	}
	@Override
	protected Class<Emissor> getEntityClass() {
		// TODO Auto-generated method stub
		return Emissor.class;
	}

}
