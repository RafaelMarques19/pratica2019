package Cartoes.Dao;

import Cartoes.DTO.LancamentoDTO;
import Cartoes.Entity.Lancamento;


public class LancamentoDAO  extends AbstractDAO<Lancamento>{
	private static final CartaoDAO CARTAO_DAO = new CartaoDAO();
	private static final EmissorDAO EMISSOR_DAO = new EmissorDAO();
	private static final LojaDAO LOJA_DAO = new LojaDAO();


	
	public Lancamento parseFrom(LancamentoDTO dto) {
		Lancamento lancamentos = new Lancamento();
		if(dto.getIdLancamento() != null) {
			lancamentos = buscar(dto.getIdLancamento());
		}
		lancamentos.setDescricao(dto.getDescricao());
		lancamentos.setValor(dto.getValor());
		lancamentos.setDataCompra(dto.getDataCompra());
		lancamentos.setCartoes(CARTAO_DAO.parseFrom(dto.getCartoes()));
		lancamentos.setEmissores(EMISSOR_DAO.parseFrom(dto.getEmissores()));
		lancamentos.setLojas(LOJA_DAO.parseFrom(dto.getLojas()));
		
		
		
		return lancamentos;
	}
	
	
	@Override
	protected Class<Lancamento> getEntityClass() {
		// TODO Auto-generated method stub
		return Lancamento.class;
	}

}
