package Cartoes.Dao;

import Cartoes.DTO.LojaDTO;
import Cartoes.Entity.Loja;

public class LojaDAO extends AbstractDAO<Loja> {
	
	public Loja parseFrom(LojaDTO dto) {
		Loja lojas = new Loja();
		if (dto.getIdLoja() != null) {
			lojas = buscar(dto.getIdLoja());
		}else {
			
		}
		lojas.setNome(dto.getNome());
		return lojas;
	}
	

	@Override
	protected Class<Loja> getEntityClass() {
		// TODO Auto-generated method stub
		return Loja.class;
	}

}
