package Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@SequenceGenerator(allocationSize = 1, name = "CARTOES_SEQ", sequenceName = "CARTOES_SEQ")
@Table(name = "CARTOES")

public class Cartao extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "CARTOES_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_CARTAO")
	private Integer id;
	
	@Column(name = "CHIP")
	private String chip;
	
	@Column(name = "VENCIMENTO")
	private String vencimento;
	
	@ManyToOne
	@JoinColumn(name = "FK_ID_CLIENTE")
    private Cliente clientes;
	
	@ManyToOne
	@JoinColumn(name = "FK_ID_BANDEIRA")
	private Bandeira bandeiras;
	
	@ManyToOne
	@JoinColumn(name = "FK_ID_EMISSOR")
	private Emissor emissores;

	
	@OneToMany(mappedBy = "cartoes", cascade = CascadeType.ALL)
	private List<Lancamento> lancamentos = new ArrayList<>();
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getChip() {
		return chip;
	}

	public void setChip(String chip) {
		this.chip = chip;
	}

	public String getVencimento() {
		return vencimento;
	}

	public void setVencimento(String vencimento) {
		this.vencimento = vencimento;
	}

	public Cliente getClientes() {
		return clientes;
	}

	public void setClientes(Cliente clientes) {
		this.clientes = clientes;
	}

	public Bandeira getBandeiras() {
		return bandeiras;
	}

	public void setBandeiras(Bandeira bandeiras) {
		this.bandeiras = bandeiras;
	}

	public Emissor getEmissores() {
		return emissores;
	}

	public void setEmissores(Emissor emissores) {
		this.emissores = emissores;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

}
