package Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@SequenceGenerator(allocationSize = 1, name = "CREDENCIADOR_SEQ", sequenceName = "CREDENCIADOR_SEQ")
@Table(name = "CREDENCIADOR")

public class Credenciador extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "CREDENCIADOR_SEQ", strategy = GenerationType.SEQUENCE )
	@Column(name = "ID_CREDENCIADOR")
	private Integer id;
	
	@Column(name = "NOME")
	private String nome;
	
	@OneToMany(mappedBy = "credenciadores", cascade = CascadeType.ALL)
	private List<LojaCredenciador> lojaCredenciadores = new ArrayList();
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
	
	

}
