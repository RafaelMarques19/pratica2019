package Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@SequenceGenerator(allocationSize = 1, name = "LANCAMENTOS_SEQ",sequenceName = "LANCAMENTOS_SEQ")
@Table(name = "LANCAMENTOS")


public class Lancamento extends AbstractEntity {
	@Id
	@GeneratedValue(generator = "LANCAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_LANCAMENTO")
	private Integer id;
	
	@Column(name = "DESCRICAO")
	private String descricao;
	
	@Column(name = "VALOR")
	private double valor;
	
	@Column(name = "DATA_COMPRA")
	private String DataCompra;
	
	@ManyToOne
	@JoinColumn(name = "FK_ID_CARTAO")
	private Cartao cartoes;
	
	@ManyToOne
	@JoinColumn(name = "FK_ID_LOJA")
	private Loja lojas;
	
	@ManyToOne
	@JoinColumn(name = "FK_ID_EMISSOR")
	private Emissor emissores;
	
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getDataCompra() {
		return DataCompra;
	}

	public void setDataCompra(String dataCompra) {
		DataCompra = dataCompra;
	}

	public Cartao getCartoes() {
		return cartoes;
	}

	public void setCartoes(Cartao cartoes) {
		this.cartoes = cartoes;
	}

	public Loja getLojas() {
		return lojas;
	}

	public void setLojas(Loja lojas) {
		this.lojas = lojas;
	}

	public Emissor getEmissores() {
		return emissores;
	}

	public void setEmissores(Emissor emissores) {
		this.emissores = emissores;
	}

	
	
	
	
	
	
}
