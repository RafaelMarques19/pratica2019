package Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

	@Entity
	@SequenceGenerator(allocationSize = 1, name = "LOJAS_SEQ", sequenceName = "LOJAS_SEQ")
	@Table(name = "LOJAS")

public class  Loja extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "LOJAS_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_LOJA")
	private Integer id;
	@Column(name = "NOME")
	private String nome;
	
	@OneToMany(mappedBy = "lojas", cascade = CascadeType.ALL)
	private List<LojaCredenciador> lojaCredenciadores = new ArrayList();
	
	@OneToMany(mappedBy ="lojas",cascade = CascadeType.ALL)
	private List<Lancamento> lancamentos = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
	
}
