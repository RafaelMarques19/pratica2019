package Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@SequenceGenerator(allocationSize = 1, name = "BANDEIRAS_SEQ", sequenceName = "BANDEIRAS_SEQ")
@Table(name = "BANDEIRAS")

public class Bandeira extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "BANDEIRAS_SEQ", strategy  = GenerationType.SEQUENCE)
	@Column(name = "ID_BANDEIRA")
	private Integer id;
	
	@Column(name = "NOME")
	private String nome;
	
	@Column(name = "TAXA")
	private double taxa;
	
	@OneToMany(mappedBy = "bandeiras",cascade = CascadeType.ALL)
	private List<Cartao> cartoes = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getTaxa() {
		return taxa;
	}

	public void setTaxa(double taxa) {
		this.taxa = taxa;
	}
	
	
	
}
