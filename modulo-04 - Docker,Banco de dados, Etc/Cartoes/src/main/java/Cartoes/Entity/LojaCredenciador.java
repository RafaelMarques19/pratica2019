package Cartoes.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@SequenceGenerator(allocationSize = 1, name = "LOJA_CREDENCIADOR_SEQ", sequenceName = "LOJA_CREDENCIADOR_SEQ")
@Table(name = "LOJA_CREDENCIADOR")

public class LojaCredenciador extends AbstractEntity {
	@Id
	@GeneratedValue(generator ="LOJA_CREDENCIADOR_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name= "ID_LOJA_CREDENCIADOR")
	private Integer id;
	@Column(name = "TAXA")
	private double taxa;

	@ManyToOne
	@JoinColumn(name = "FK_ID_LOJA")
	private Loja lojas;
	
	@ManyToOne
	@JoinColumn(name = "FK_ID_CREDENCIADOR")
	private Credenciador credenciadores;

	public Credenciador getCredenciadores() {
		return credenciadores;
	}

	public void setCredenciadores(Credenciador credenciadores) {
		this.credenciadores = credenciadores;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getTaxa() {
		return taxa;
	}

	public void setTaxa(double taxa) {
		this.taxa = taxa;
	}

	
	
}
