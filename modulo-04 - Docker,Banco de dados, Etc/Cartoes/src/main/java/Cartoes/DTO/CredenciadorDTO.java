package Cartoes.DTO;

public class CredenciadorDTO {
	
	private Integer idCredenciador;
	private String nome;
	
	
	
	public Integer getIdCredenciador() {
		return idCredenciador;
	}
	public void setIdCredenciador(Integer idCredenciador) {
		this.idCredenciador = idCredenciador;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

}
