package Cartoes.DTO;

public class BandeiraDTO {
	
	private Integer idBandeira;
	private String nome;
	private double taxa;
	
	
	public Integer getIdBandeira() {
		return idBandeira;
	}
	public void setIdBandeira(Integer idBandeira) {
		this.idBandeira = idBandeira;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getTaxa() {
		return taxa;
	}
	public void setTaxa(double taxa) {
		this.taxa = taxa;
	}
	
	

}
