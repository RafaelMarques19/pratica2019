package Cartoes.DTO;

public class EmissorDTO {
	
	private Integer idEmissor;
	private String nome;
	private double taxa;
	
	
	public Integer getIdEmissor() {
		return idEmissor;
	}
	public void setIdEmissor(Integer idEmissor) {
		this.idEmissor = idEmissor;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getTaxa() {
		return taxa;
	}
	public void setTaxa(double taxa) {
		this.taxa = taxa;
	}
	
	
	

}
