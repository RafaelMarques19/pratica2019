package Cartoes.DTO;

import Cartoes.Entity.Emissor;

public class LancamentoDTO {
	private Integer idLancamento;
	private String descricao;
	private double valor;
	private String DataCompra;
	private CartaoDTO cartoes;
	private LojaDTO lojas;
	private EmissorDTO emissores;
	
	
	
	
	public Integer getIdLancamento() {
		return idLancamento;
	}
	public void setIdLancamento(Integer idLancamento) {
		this.idLancamento = idLancamento;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public String getDataCompra() {
		return DataCompra;
	}
	public void setDataCompra(String dataCompra) {
		DataCompra = dataCompra;
	}
	public CartaoDTO getCartoes() {
		return cartoes;
	}
	public void setCartoes(CartaoDTO cartoes) {
		this.cartoes = cartoes;
	}
	public LojaDTO getLojas() {
		return lojas;
	}
	public void setLojas(LojaDTO lojas) {
		this.lojas = lojas;
	}
	public EmissorDTO getEmissores() {
		return emissores;
	}
	public void setEmissores(EmissorDTO emissores) {
		this.emissores = emissores;
	}
	
	
	
	
	
}
