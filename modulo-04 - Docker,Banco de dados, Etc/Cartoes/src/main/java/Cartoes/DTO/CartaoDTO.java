package Cartoes.DTO;

public class CartaoDTO {
	private Integer idCartao;
	private String chip;
	private String vencimento;
	private ClienteDTO clientes;
	private BandeiraDTO bandeiras;
	private EmissorDTO emissores;
	
	
	
	public Integer getIdCartao() {
		return idCartao;
	}
	public void setIdCartao(Integer idCartao) {
		this.idCartao = idCartao;
	}
	public String getChip() {
		return chip;
	}
	public void setChip(String chip) {
		this.chip = chip;
	}
	public String getVencimento() {
		return vencimento;
	}
	public void setVencimento(String vencimento) {
		this.vencimento = vencimento;
	}
	public ClienteDTO getClientes() {
		return clientes;
	}
	public void setClientes(ClienteDTO clientes) {
		this.clientes = clientes;
	}
	public BandeiraDTO getBandeiras() {
		return bandeiras;
	}
	public void setBandeiras(BandeiraDTO bandeiras) {
		this.bandeiras = bandeiras;
	}
	public EmissorDTO getEmissores() {
		return emissores;
	}
	public void setEmissores(EmissorDTO emissores) {
		this.emissores = emissores;
	}
	
	
	
	
	
}
