package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.InventarioXItem;
import br.com.dbccompany.vemserSpring.Service.InventarioXItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/IXI")
public class InventarioXItemController {


    @Autowired
    InventarioXItemService service;

    @PostMapping(value = "/novo")
    @ResponseBody
    public InventarioXItem novoInv(@RequestBody InventarioXItem ixi){
        return service.salvar(ixi);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public InventarioXItem editarIxi(@PathVariable Integer id, @RequestBody InventarioXItem ixi ){
      return  service.editar(id, ixi);
    }
}
