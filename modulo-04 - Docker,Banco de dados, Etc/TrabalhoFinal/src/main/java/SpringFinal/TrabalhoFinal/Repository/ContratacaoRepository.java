package SpringFinal.TrabalhoFinal.Repository;

import SpringFinal.TrabalhoFinal.Entity.Contratacao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContratacaoRepository extends CrudRepository<Contratacao, Integer> {

}
