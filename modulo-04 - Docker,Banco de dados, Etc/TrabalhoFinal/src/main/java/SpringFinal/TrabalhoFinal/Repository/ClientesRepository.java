package SpringFinal.TrabalhoFinal.Repository;


import SpringFinal.TrabalhoFinal.Entity.Clientes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientesRepository extends CrudRepository<Clientes, Integer> {

    Clientes findByNome(String nome);

    Clientes findByCpf(String cpf);

}
