package SpringFinal.TrabalhoFinal.Repository;

import SpringFinal.TrabalhoFinal.Entity.Usuarios;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuariosRepository extends CrudRepository<Usuarios, Integer> {

    Usuarios findByEmail(String email);

    List<Usuarios> findAllByNome(String nome);

    Usuarios findByUsername(String username);

}
