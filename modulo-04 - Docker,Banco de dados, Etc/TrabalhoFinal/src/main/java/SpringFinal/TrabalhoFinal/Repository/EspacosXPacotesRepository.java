package SpringFinal.TrabalhoFinal.Repository;


import SpringFinal.TrabalhoFinal.Entity.EspacosXPacotes;
import SpringFinal.TrabalhoFinal.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacosXPacotesRepository extends CrudRepository<EspacosXPacotes, Integer> {

    List<EspacosXPacotes> findAllByPacote(Pacotes pacote);

}