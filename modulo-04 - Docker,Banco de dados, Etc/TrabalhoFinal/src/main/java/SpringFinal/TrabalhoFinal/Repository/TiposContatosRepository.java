package SpringFinal.TrabalhoFinal.Repository;

import SpringFinal.TrabalhoFinal.Entity.TiposContatos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TiposContatosRepository extends CrudRepository<TiposContatos, Integer> {

    TiposContatos findByNome(String nome);

}
