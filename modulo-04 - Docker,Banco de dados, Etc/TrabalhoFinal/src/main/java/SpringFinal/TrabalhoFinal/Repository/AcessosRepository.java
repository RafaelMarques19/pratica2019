package SpringFinal.TrabalhoFinal.Repository;


import SpringFinal.TrabalhoFinal.Entity.Acessos;
import SpringFinal.TrabalhoFinal.Entity.Clientes;
import SpringFinal.TrabalhoFinal.Entity.Espacos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AcessosRepository extends CrudRepository<Acessos, Integer> {

    List<Acessos> findAllBySaldoXCliente_IdClienteAndSaldoXCliente_IdEspaco(Clientes cliente, Espacos espaco);

    List<Acessos> findAllByData(LocalDateTime data);

}
