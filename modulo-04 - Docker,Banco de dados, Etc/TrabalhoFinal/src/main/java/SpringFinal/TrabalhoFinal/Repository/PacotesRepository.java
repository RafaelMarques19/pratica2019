package SpringFinal.TrabalhoFinal.Repository;


import SpringFinal.TrabalhoFinal.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacotesRepository extends CrudRepository<Pacotes, Integer> {

}
