package SpringFinal.TrabalhoFinal.Repository;


import SpringFinal.TrabalhoFinal.Entity.ClientesXPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientesXPacotesRepository extends CrudRepository<ClientesXPacotes, Integer> {

}
