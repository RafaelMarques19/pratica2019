package SpringFinal.TrabalhoFinal.Repository;

import SpringFinal.TrabalhoFinal.Entity.Contatos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContatosRepository extends CrudRepository<Contatos, Integer> {

}
