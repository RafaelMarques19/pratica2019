package SpringFinal.TrabalhoFinal.Repository;



import SpringFinal.TrabalhoFinal.Entity.Espacos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacosRepository extends CrudRepository<Espacos, Integer> {

    Espacos findByNome(String nome);

}