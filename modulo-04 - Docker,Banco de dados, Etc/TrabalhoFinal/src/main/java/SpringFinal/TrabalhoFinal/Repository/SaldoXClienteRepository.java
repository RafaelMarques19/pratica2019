package SpringFinal.TrabalhoFinal.Repository;

import SpringFinal.TrabalhoFinal.Entity.SaldoXCliente;

import SpringFinal.TrabalhoFinal.Entity.SaldoXClienteID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaldoXClienteRepository extends CrudRepository<SaldoXCliente, SaldoXClienteID> {

    List<SaldoXCliente> findAllByIdEspaco(Integer idEspaco);

    List<SaldoXCliente> findAllByIdCliente(Integer idCliente);

    SaldoXCliente findByQuantidade (int quantidade);
}

