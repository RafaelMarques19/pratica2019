package SpringFinal.TrabalhoFinal.Service;



import SpringFinal.TrabalhoFinal.Entity.AbstractEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class AbstractService <
        Entidade extends AbstractEntity,
        EntidadeRepository extends CrudRepository<Entidade, Integer>> {
    @Autowired
    protected EntidadeRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Entidade salvar( Entidade entidade ) throws Exception {
        return repository.save(entidade);
    }

    @Transactional( rollbackFor = Exception.class )
    public Entidade editar( Integer id, Entidade entidade ) {
        entidade.setId(id);
        return repository.save(entidade);
    }

    public List<Entidade> listarTodos() {
        return (List<Entidade>) repository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public boolean deletarPorId(Integer id){
        Entidade entidade = repository.findById(id).get();
        boolean existia = entidade == null ? false : true;
        repository.deleteById(id);
        return existia;
    }

    public Entidade buscarPorId(Integer id){
        return repository.findById(id).get();
    }
}
