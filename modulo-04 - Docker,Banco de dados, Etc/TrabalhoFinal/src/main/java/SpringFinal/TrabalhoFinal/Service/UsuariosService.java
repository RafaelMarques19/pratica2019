package SpringFinal.TrabalhoFinal.Service;


import SpringFinal.TrabalhoFinal.Entity.Usuarios;
import SpringFinal.TrabalhoFinal.Repository.UsuariosRepository;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("userDetailsService")
public class UsuariosService extends AbstractService<Usuarios, UsuariosRepository>   {
    @Override
    public Usuarios salvar(Usuarios usuario) throws Exception {
        String senha = usuario.getPassword();

        if(senha.length() < 6){
            throw new Exception("Senha tem menos de 6 caracteres");
        }

        if(!checkEmail(usuario.getEmail())){
            throw new Exception("Email inválido");
        }

        usuario.setPassword(Criptografia.criptografar(usuario.getPassword()));
        return repository.save(usuario);
    }

    public Usuarios buscarPorUsername(String username){
        return repository.findByUsername(username);
    }


    public static boolean checkEmail(String email) {
        boolean isEmailIdValid = false;
        if (email != null && email.length() > 0) {
            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
            if (matcher.matches()) {
                isEmailIdValid = true;
            }
        }
        return isEmailIdValid;
    }


}
