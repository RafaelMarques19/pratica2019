package SpringFinal.TrabalhoFinal.Service;


import SpringFinal.TrabalhoFinal.Entity.TiposContatos;
import SpringFinal.TrabalhoFinal.Repository.TiposContatosRepository;
import org.springframework.stereotype.Service;

@Service
public class TiposContatosService extends AbstractService<TiposContatos, TiposContatosRepository> {
    public TiposContatos buscarPorNome(String nome){
        return repository.findByNome(nome);
    }
}