package SpringFinal.TrabalhoFinal.Service;


import java.text.NumberFormat;
import java.util.Locale;
import SpringFinal.TrabalhoFinal.Entity.Espacos;
import SpringFinal.TrabalhoFinal.Repository.EspacosRepository;
import org.springframework.stereotype.Service;

@Service
public class EspacosService extends AbstractService<Espacos, EspacosRepository>{

    public Espacos buscarPorNome(String nome){
        Espacos espaco = repository.findByNome(nome);
        espaco.setValorReais(toReais(espaco.getValor()));
        return espaco;
    }

    @Override
    public Espacos salvar(Espacos espaco) throws Exception {
        espaco.setValor(toDouble(espaco.getValorReais()));
        return repository.save(espaco);
    }

    @Override
    public Espacos buscarPorId(Integer id) {
        Espacos espaco =  super.buscarPorId(id);
        espaco.setValorReais(toReais(espaco.getValor()));
        return espaco;
    }

        public static String toReais(double valor){
            NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
            return nf.format(valor);
        }

        public static Double toDouble(String valor){
            return Double.parseDouble(valor);
        }
    }


