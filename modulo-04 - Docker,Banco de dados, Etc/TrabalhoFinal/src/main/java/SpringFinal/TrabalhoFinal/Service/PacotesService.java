package SpringFinal.TrabalhoFinal.Service;

import SpringFinal.TrabalhoFinal.Entity.Pacotes;
import SpringFinal.TrabalhoFinal.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PacotesService extends AbstractService<Pacotes, PacotesRepository>{

    @Autowired
    EspacosService espacosService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Pacotes salvar(Pacotes pacote) throws Exception{
        String[] array = pacote.getValorReais().split(" ");
        pacote.setValor(espacosService.toDouble(pacote.getValorReais()));
        return this.repository.save(pacote);
    }

    @Override
    public Pacotes buscarPorId(Integer id) {
        Pacotes pacote = super.buscarPorId(id);
        pacote.setValorReais(espacosService.toReais(pacote.getValor()));
        return pacote;
    }
}
