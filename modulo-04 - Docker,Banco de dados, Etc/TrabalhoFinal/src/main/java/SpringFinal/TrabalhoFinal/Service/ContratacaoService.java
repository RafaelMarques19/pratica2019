package SpringFinal.TrabalhoFinal.Service;


import SpringFinal.TrabalhoFinal.Entity.Contratacao;
import SpringFinal.TrabalhoFinal.Entity.Espacos;
import SpringFinal.TrabalhoFinal.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContratacaoService extends AbstractService<Contratacao, ContratacaoRepository>{

    @Autowired
    EspacosService espacoService;

    @Override
    public List<Contratacao> listarTodos() {
        return (List<Contratacao>) repository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public String salvarString(Contratacao contratacao) {
        contratacao.setDesconto(espacoService.toDouble(contratacao.getDescontoReais()));
        Espacos espaco = null;
        if(contratacao.getEspaco().getId() != 0) {
            espaco = espacoService.buscarPorId(contratacao.getEspaco().getId());
        }
        Contratacao con = repository.save(contratacao);
        double diariaEspaco = 0;
        if(espaco != null) {
            diariaEspaco = espaco.getValor();
        }
        double valorTabelado = 0;
        switch (contratacao.getTipoContratacao()) {
            case MES: valorTabelado = diariaEspaco * 30; break;
            case SEMANA: valorTabelado = diariaEspaco * 7; break;
            case DIARIA: valorTabelado = diariaEspaco; break;
            case TURNO: valorTabelado = diariaEspaco * ( 5/24 ); break;
            case HORA: valorTabelado = diariaEspaco / 24;
            case MINUTO: valorTabelado = diariaEspaco / 1440; break;
            default: return "";
        }
        double custo = valorTabelado * con.getQuantidade() - con.getDesconto();
        return espacoService.toReais(custo);
    }

}

