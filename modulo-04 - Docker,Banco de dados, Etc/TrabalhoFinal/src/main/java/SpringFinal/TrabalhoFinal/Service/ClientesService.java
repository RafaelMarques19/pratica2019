package SpringFinal.TrabalhoFinal.Service;




import SpringFinal.TrabalhoFinal.Entity.Clientes;
import SpringFinal.TrabalhoFinal.Entity.Contatos;
import SpringFinal.TrabalhoFinal.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientesService extends AbstractService<Clientes, ClientesRepository> {

    @Autowired
    ContatosService contatoService = new ContatosService();

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Clientes salvar(Clientes cliente) throws Exception {

        boolean email = false;
        boolean telefone = false;

        if(repository.findByCpf(cliente.getCpf()) != null) {
            throw new Exception("CPF já cadastrado!");
        }

        for (Contatos contato : cliente.getContatos()) {
            String nome = contato.getTipoContato().getNome().toLowerCase();
            if(nome == "email") email = true;
            if(nome == "telefone") telefone = true;
        }
        if(! (telefone && email)){
            throw new Exception("Deve ser informado ao menos um email e um telefone!");
        }

        return super.salvar(cliente);
    }

    public Clientes buscarPorNome(String nome) {
        return repository.findByNome(nome);
    }

    public Clientes buscarPorCpf(String cpf) {
        return repository.findByCpf(cpf);
    }
}
