package SpringFinal.TrabalhoFinal.Service;


import SpringFinal.TrabalhoFinal.Entity.SaldoXCliente;
import SpringFinal.TrabalhoFinal.Entity.SaldoXClienteID;

import SpringFinal.TrabalhoFinal.Repository.SaldoXClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class SaldoXClienteService {

    @Autowired
    private SaldoXClienteRepository saldoXClienteRepository;

    @Autowired
    private ClientesService clientesService;

    @Autowired
    private EspacosService espacosService;



    @Transactional(rollbackFor = Exception.class)
    public SaldoXCliente salvar(SaldoXCliente saldoXCliente){
        return saldoXClienteRepository.save(saldoXCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoXCliente editar(Integer idCliente, Integer idEspaco, SaldoXCliente saldoXCliente) {
        SaldoXClienteID idClienteXSaldo = new SaldoXClienteID();
        idClienteXSaldo.setCliente(clientesService.buscarPorId(idCliente));
        idClienteXSaldo.setEspaco(espacosService.buscarPorId(idEspaco));
        saldoXCliente.setId(idClienteXSaldo);
        return saldoXClienteRepository.save(saldoXCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoXCliente editarPorClienteXSaldoID(SaldoXClienteID id, SaldoXCliente saldoXCliente) {
        saldoXCliente.setId(id);
        return saldoXClienteRepository.save(saldoXCliente);
    }


    public List<SaldoXCliente> todosClientesXSaldos(){
        return (List<SaldoXCliente>) saldoXClienteRepository.findAll();

    }

    @Transactional(rollbackFor = Exception.class)
    public boolean deletarPorId(Integer idCliente, Integer idEspaco) {
        SaldoXClienteID id = new SaldoXClienteID();
        id.setCliente(clientesService.buscarPorId(idCliente));
        id.setEspaco(espacosService.buscarPorId(idEspaco));
        boolean existia = saldoXClienteRepository.findById(id).isPresent();
        saldoXClienteRepository.deleteById(id);
        return existia;
    }

    public SaldoXCliente editarPorSaldoXClienteId(SaldoXClienteID id, SaldoXCliente sc) {
        sc.setId(id);
        return saldoXClienteRepository.save(sc);
    }

    public SaldoXCliente buscarPorId(Integer idCliente, Integer idEspaco) {
        SaldoXClienteID id = new SaldoXClienteID();
        id.setCliente(clientesService.buscarPorId(idCliente));
        id.setEspaco(espacosService.buscarPorId(idEspaco));
        return saldoXClienteRepository.findById(id).get();
    }

    public LocalDateTime calcularDataVencimento(Integer data){
        LocalDateTime agora = LocalDateTime.now();
        return agora.plusDays(data);
    }

}
