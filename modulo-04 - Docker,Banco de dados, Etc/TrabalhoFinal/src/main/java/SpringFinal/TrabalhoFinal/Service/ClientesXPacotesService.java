package SpringFinal.TrabalhoFinal.Service;

import SpringFinal.TrabalhoFinal.Entity.ClientesXPacotes;
import SpringFinal.TrabalhoFinal.Repository.ClientesXPacotesRepository;
import org.springframework.stereotype.Service;

@Service
public class ClientesXPacotesService extends AbstractService<ClientesXPacotes, ClientesXPacotesRepository> {

}
