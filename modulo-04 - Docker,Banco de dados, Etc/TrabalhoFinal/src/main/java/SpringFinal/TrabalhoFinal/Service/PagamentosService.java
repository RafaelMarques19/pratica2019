package SpringFinal.TrabalhoFinal.Service;


import SpringFinal.TrabalhoFinal.Entity.*;
import SpringFinal.TrabalhoFinal.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class PagamentosService extends AbstractService<Pagamentos, PagamentosRepository> {

    @Autowired
    ContratacaoService contratacaoService;
    @Autowired
    ClientesXPacotesService clienteXPacoteService;
    @Autowired
    EspacosXPacotesService espacoXPacoteService;
    @Autowired
    SaldoXClienteService saldoXClienteService;
    @Autowired
    ClientesService clienteService;
    @Autowired
    EspacosService espacoService;

    @Override
    @Transactional( rollbackFor = Exception.class )
    public Pagamentos salvar(Pagamentos pagamento) throws Exception {

        if(pagamento.getClienteXPacote() != null) {
            ClientesXPacotes clienteXPacote = clienteXPacoteService.buscarPorId(pagamento.getClienteXPacote().getId());
            Clientes cliente = clienteXPacote.getCliente();
            Pacotes pacote = clienteXPacote.getPacote();
            List<EspacosXPacotes> espacoXPacotes = espacoXPacoteService.buscarPorPacote(pacote);
            for(EspacosXPacotes e : espacoXPacotes) {
                SaldoXClienteID id = new SaldoXClienteID();
                id.setCliente(cliente);
                id.setEspaco(e.getEspaco());
                SaldoXCliente sc = new SaldoXCliente();
                sc.setId(id);
                sc.setQuantidade(clienteXPacote.getQuantidade());
                sc.setVencimento(saldoXClienteService.calcularDataVencimento(e.getPrazo()));
                sc.setTipoContratacao(e.getTipoContratacao());
                saldoXClienteService.salvar(sc);
            }
        } else if(pagamento.getContratacao() != null) {
            SaldoXClienteID id = new SaldoXClienteID();
            SaldoXCliente sc = new SaldoXCliente();
            Contratacao contratacao = contratacaoService.buscarPorId(pagamento.getContratacao().getId());
            pagamento.setContratacao(contratacao);
            Integer idCliente = contratacao.getCliente().getId();
            Integer idEspaco = contratacao.getEspaco().getId();
            id.setCliente(clienteService.buscarPorId(idCliente));
            id.setEspaco(espacoService.buscarPorId(idEspaco));
            sc.setId(id);
            sc.setQuantidade(contratacao.getQuantidade());
            sc.setTipoContratacao(contratacao.getTipoContratacao());
            sc.setVencimento(saldoXClienteService.calcularDataVencimento(contratacao.getPrazo()));
            if(saldoXClienteService.buscarPorId(id.getCliente().getId(), id.getEspaco().getId()) != null){
                saldoXClienteService.salvar(sc);
            } else {
                saldoXClienteService.editar(idCliente, idEspaco, sc);
            }
        }
        return repository.save(pagamento);
    }

}
