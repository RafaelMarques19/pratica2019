package SpringFinal.TrabalhoFinal.Service;


import SpringFinal.TrabalhoFinal.Entity.*;
import SpringFinal.TrabalhoFinal.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class AcessosService extends AbstractService<Acessos, AcessosRepository>{

    @Autowired
    private SaldoXClienteService saldoClienteService;

    @Autowired
    private ClientesService clienteService;

    @Autowired
    private EspacosService espacoService;

    @Transactional(rollbackFor = Exception.class)
    public String acessar(Acessos acesso) {
        Integer idCliente = acesso.getSaldoXCliente().getId().getCliente().getId();
        Integer idEspaco = acesso.getSaldoXCliente().getId().getEspaco().getId();
        Clientes cliente = clienteService.buscarPorId(idCliente);
        Espacos espaco = espacoService.buscarPorId(idEspaco);
        List<Acessos> acessos = repository.findAllBySaldoXCliente_IdClienteAndSaldoXCliente_IdEspaco(cliente, espaco);
        SaldoXCliente saldo;
        Acessos ultimoAcesso = new Acessos();

        if(acessos.size() > 0){
            ultimoAcesso = acessos.get(acessos.size() - 1);
            if(ultimoAcesso.isEntrada()){
                acesso.setEntrada(false); }
            else {
                acesso.setEntrada(true);
            }
        }
        else {
            acesso.setEntrada(true);
        }

        if(acesso.getData() == null) {
            acesso.setData(LocalDateTime.now());
        }

        if(acesso.isExcecao() && acesso.isEntrada()) {
            SaldoXClienteID id = new SaldoXClienteID();
            id.setCliente(clienteService.buscarPorId(idCliente));
            id.setEspaco(espacoService.buscarPorId(idEspaco));
            saldo = new SaldoXCliente();
            saldo.setId(id);
            saldo.setQuantidade(999);
            saldo.setVencimento(LocalDateTime.now()/* + 86400000*/);
            saldo.setTipoContratacao(TipoContratacao.ESPECIAL);
            if (saldoClienteService.buscarPorId(idCliente, idEspaco) != null) {
                saldo = saldoClienteService.salvar(saldo);
            } else {
                saldo = saldoClienteService.editar(idCliente, idEspaco, saldo);
            }
            acesso.setSaldoXCliente(saldo);
            repository.save(acesso);
            return "Especial!";
        }

        if(acesso.isEntrada()){
            LocalDateTime vencimento = acesso.getSaldoXCliente().getVencimento();
            LocalDateTime dataAtual = LocalDateTime.now();

            if(acesso.getSaldoXCliente() != null && acesso.getSaldoXCliente().getQuantidade() > 0){
                if(dataAtual.isAfter(vencimento)){
                    acesso.getSaldoXCliente().setQuantidade(0);
                    return "Saldo vencido!";
                }
                repository.save(acesso);
                SaldoXCliente saldoXCliente = acesso.getSaldoXCliente();
                saldoXCliente.addAcesso(acesso);
                saldoClienteService.editarPorSaldoXClienteId(saldoXCliente.getId(), acesso.getSaldoXCliente());
                return "Saldo:" + acesso.getSaldoXCliente().getVencimento();
            }
            else{
                return "Saldo Insuficiente";
            }
        }
        else{
            LocalDateTime horaEntrada = ultimoAcesso.getData();
            long minutes = ChronoUnit.MINUTES.between(horaEntrada, acesso.getData());
            long hours = ChronoUnit.HOURS.between(horaEntrada, acesso.getData());
            long days = ChronoUnit.DAYS.between(horaEntrada, acesso.getData());
            long weeks = ChronoUnit.WEEKS.between(horaEntrada, acesso.getData());
            SaldoXCliente novoSaldo = new SaldoXCliente();
            novoSaldo.setQuantidade(acesso.getSaldoXCliente().getQuantidade());
            novoSaldo.setId(acesso.getSaldoXCliente().getId());
            novoSaldo.setTipoContratacao(acesso.getSaldoXCliente().getTipoContratacao());
            novoSaldo.setVencimento(acesso.getSaldoXCliente().getVencimento().minusHours(hours).minusMinutes(minutes).minusDays(days).minusWeeks(weeks));
            saldoClienteService.salvar(novoSaldo);
        }
        repository.save(acesso);
        return "Acesso salvo";
    }

    public Acessos buscarAcesso(Integer id) {
        return repository.findById(id).get();
    }

    @Transactional(rollbackFor = Exception.class)
    public Acessos editar(Integer id, Acessos acesso) {
        acesso.setId(id);
        return repository.save(acesso);
    }

    public List<Acessos> listarTodos(){
        return (List<Acessos>) repository.findAll();
    }

    public List<Acessos> buscarPorData(LocalDateTime data) {
        return (List<Acessos>) repository.findAllByData(data);
    }
}
