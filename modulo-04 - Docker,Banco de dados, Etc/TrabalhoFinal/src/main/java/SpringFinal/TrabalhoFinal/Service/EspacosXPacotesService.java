package SpringFinal.TrabalhoFinal.Service;



import SpringFinal.TrabalhoFinal.Entity.EspacosXPacotes;
import SpringFinal.TrabalhoFinal.Entity.Pacotes;
import SpringFinal.TrabalhoFinal.Repository.EspacosXPacotesRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EspacosXPacotesService extends AbstractService<EspacosXPacotes, EspacosXPacotesRepository> {

    public List<EspacosXPacotes> buscarPorPacote(Pacotes pacote){
        return (List<EspacosXPacotes>) repository.findAllByPacote(pacote);
    }

}
