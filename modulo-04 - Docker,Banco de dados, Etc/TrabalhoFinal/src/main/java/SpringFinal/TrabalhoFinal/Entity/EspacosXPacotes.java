package SpringFinal.TrabalhoFinal.Entity;

import SpringFinal.TrabalhoFinal.Entity.AbstractEntity;
import javax.persistence.*;
import java.time.LocalDateTime;

import javax.persistence.*;

@Entity
@Table( name = "espacos_x_pacotes" )
@SequenceGenerator( allocationSize = 1, name = "ESPACOS_X_PACOTES_SEQ", sequenceName = "ESPACOS_X_PACOTES_SEQ" )
public class EspacosXPacotes extends AbstractEntity {

    @Id
    @Column( name = "id_espaco_pacote" )
    @GeneratedValue(  generator = "ESPACOS_X_PACOTES_SEQ", strategy = GenerationType.SEQUENCE  )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "fk_id_espaco")
    private Espacos espaco;

    @ManyToOne
    @JoinColumn( name = "fk_id_pacote" )
    private Pacotes pacote;

    @Column( name = "tipo_contratacao" )
    @Enumerated( EnumType.STRING )
    private TipoContratacao tipoContratacao;

    @Column( name = "quantidade" )
    private Integer quantidade;

    @Column( name = "prazo" )
    private Integer prazo;

    // GETTERS AND SETTERS

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public Pacotes getPacote() {
        return pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
