package SpringFinal.TrabalhoFinal.Entity;

import javax.persistence.*;

@Entity
@Table( name = "contratacoes" )
@SequenceGenerator( allocationSize = 1, name = "CONTRATACOES_SEQ", sequenceName = "CONTRATACOES_SEQ" )
public class Contratacao extends AbstractEntity {

    @Id
    @Column( name = "id_contratacao" )
    @GeneratedValue(  generator = "CONTRATACOES_SEQ", strategy = GenerationType.SEQUENCE  )
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_espaco")
    private Espacos espaco;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_cliente")
    private Clientes cliente;

    @Column(name = "tipo_contratacao")
    @Enumerated( EnumType.STRING )
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private Integer quantidade = 0;

    private double desconto = 0.0;

    @Transient
    private String descontoReais = "";

    @Column(nullable = false)
    private Integer prazo = 0;

    // GETTERS AND SETTERS

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public String getDescontoReais() {
        return descontoReais;
    }

    public void setDescontoReais(String descontoReais) {
        this.descontoReais = descontoReais;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
