package SpringFinal.TrabalhoFinal.Entity;

import javax.persistence.*;



import javax.persistence.*;

@Entity
@Table( name = "pagamentos" )
@SequenceGenerator( allocationSize = 1, name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ" )
public class Pagamentos extends AbstractEntity {

    @Id
    @Column( name = "id_pagamento" )
    @GeneratedValue( generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_cliente_pacote")
    private ClientesXPacotes clienteXPacote;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_contratacao")
    private Contratacao contratacao;

    @Column(name = "tipo_pagamento")
    @Enumerated( EnumType.STRING )
    private TipoPagamento tipoPagamento;

    // GETTERS AND SETTERS

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesXPacotes getClienteXPacote() {
        return clienteXPacote;
    }

    public void setClienteXPacote(ClientesXPacotes clienteXPacote) {
        this.clienteXPacote = clienteXPacote;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
