package SpringFinal.TrabalhoFinal.Entity;

import javax.persistence.*;

@Entity
@Table( name = "usuarios" )
@SequenceGenerator( allocationSize = 1, name = "USUARIOS_SEQ", sequenceName = "USUARIOS_SEQ" )
public class Usuarios extends AbstractEntity {
    @Id
    @Column( name = "id_usuario" )
    @GeneratedValue(  generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE  )
    private Integer id;

    private String nome;

    @Column(unique = true)
    private String email;

    @Column(unique = true, name = "login")
    private String username;

    @Column(name = "senha")
    private String password;

    // GETTERS AND SETTERS

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

