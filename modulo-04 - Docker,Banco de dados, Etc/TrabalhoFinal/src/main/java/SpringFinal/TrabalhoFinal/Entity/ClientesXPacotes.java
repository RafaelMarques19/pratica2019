package SpringFinal.TrabalhoFinal.Entity;

import javax.persistence.*;

import javax.persistence.*;

@Entity
@Table( name = "clientes_x_pacotes" )
@SequenceGenerator( allocationSize = 1, name = "CLIENTES_X_PACOTES_SEQ", sequenceName = "CLIENTES_X_PACOTES_SEQ" )
public class ClientesXPacotes extends AbstractEntity {

    @Id
    @Column( name = "id_cliente_x_pacote")
    @GeneratedValue( generator = "CLIENTES_X_PACOTES_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "fk_id_cliente")
    private Clientes cliente;

    @ManyToOne
    @JoinColumn( name = "fk_id_pacote")
    private Pacotes pacote;

    // GETTERS AND SETTERS

    @Column( name = "quantidade" )
    private Integer quantidade;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Pacotes getPacote() {
        return pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
