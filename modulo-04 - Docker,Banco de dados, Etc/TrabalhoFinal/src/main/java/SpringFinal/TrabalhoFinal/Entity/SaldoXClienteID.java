package SpringFinal.TrabalhoFinal.Entity;

import java.io.Serializable;
import javax.persistence.*;

@Embeddable
public class SaldoXClienteID implements Serializable {
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_cliente")
    private Clientes cliente;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_espaco")
    private Espacos espaco;

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    @Override
    public boolean equals(Object obj) {
        SaldoXClienteID outro = (SaldoXClienteID) obj;
        return (this.cliente.getId() == outro.getCliente().getId() &&
                this.espaco.getId() == outro.getEspaco().getId());
    }
}
