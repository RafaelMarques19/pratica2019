package SpringFinal.TrabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "espacos" )
@SequenceGenerator( allocationSize = 1, name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ" )
public class Espacos extends AbstractEntity {

    @Id
    @Column( name = "id_espaco" )
    @GeneratedValue(  generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE  )
    private Integer id;

    @Column( name = "nome", unique = true, nullable = false)
    private String nome;

    @Column( name = "quantidade_pessoas", nullable = false )
    private Integer quantidadePessoas;

    @Column( name = "valor",  nullable = false )
    private double valor;

    @Transient
    private String valorReais = "";

    @OneToMany( mappedBy = "espaco", cascade = CascadeType.ALL)
    private List<EspacosXPacotes> espacoXPacote = new ArrayList<>();

    @OneToMany(mappedBy = "id.espaco")
    private List<SaldoXCliente> saldosClientes = new ArrayList<>();

    @OneToMany(mappedBy = "espaco")
    private List<Contratacao> contratacoes = new ArrayList<>();

    // GETTERS AND SETTERS

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQuantidadePessoas() {
        return quantidadePessoas;
    }

    public void setQuantidadePessoas(Integer quantidadePessoas) {
        this.quantidadePessoas = quantidadePessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getValorReais() {
        return valorReais;
    }

    public void setValorReais(String valorReais) {
        this.valorReais = valorReais;
    }

    public List<EspacosXPacotes> getEspacoXPacote() {
        return espacoXPacote;
    }

    public void setEspacoXPacote(List<EspacosXPacotes> espacoXPacote) {
        this.espacoXPacote = espacoXPacote;
    }

    public List<SaldoXCliente> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoXCliente> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }
}
