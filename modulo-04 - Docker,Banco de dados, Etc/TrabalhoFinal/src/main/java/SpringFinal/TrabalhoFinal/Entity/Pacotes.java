package SpringFinal.TrabalhoFinal.Entity;

import SpringFinal.TrabalhoFinal.Entity.AbstractEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "pacotes" )
@SequenceGenerator( allocationSize = 1, name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ" )
public class Pacotes extends AbstractEntity {

    @Id
    @Column( name = "id_pacote" )
    @GeneratedValue( generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    private double valor = 0.0;

    @Transient
    private String valorReais = "";

    @OneToMany( mappedBy = "pacote", cascade = CascadeType.ALL)
    private List<EspacosXPacotes> espacoXPacote = new ArrayList<>();

    @OneToMany( mappedBy = "pacote", cascade = CascadeType.ALL)
    private List<ClientesXPacotes> clienteXPacote = new ArrayList<>();

    // GETTERS AND SETTERS

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getValorReais() {
        return valorReais;
    }

    public void setValorReais(String valorReais) {
        this.valorReais = valorReais;
    }

    public List<EspacosXPacotes> getEspacoXPacote() {
        return espacoXPacote;
    }

    public void setEspacoXPacote(List<EspacosXPacotes> espacoXPacote) {
        this.espacoXPacote = espacoXPacote;
    }

    public List<ClientesXPacotes> getClienteXPacote() {
        return clienteXPacote;
    }

    public void setClienteXPacote(List<ClientesXPacotes> clienteXPacote) {
        this.clienteXPacote = clienteXPacote;
    }
}
