package SpringFinal.TrabalhoFinal.Entity;


import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table( name = "acessos" )
@SequenceGenerator( allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ" )
public class Acessos extends AbstractEntity {

    @Id
    @Column( name = "id_acesso" )
    @GeneratedValue(  generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE  )
    private Integer id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumns({
            @JoinColumn(name = "fk_id_espaco_saldo_x_cliente", referencedColumnName = "id_espaco"),
            @JoinColumn(name = "fk_id_cliente_saldo_x_cliente", referencedColumnName = "id_cliente")
    })
    private SaldoXCliente saldoXCliente;

    @Column(name = "is_entrada")
    private boolean isEntrada = false;

    @Column(name = "is_excecao")
    private boolean isExcecao = false;

    private LocalDateTime data = LocalDateTime.now();

    // GETTERS AND SETTERS

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoXCliente getSaldoXCliente() {
        return saldoXCliente;
    }

    public void setSaldoXCliente(SaldoXCliente saldoXCliente) {
        this.saldoXCliente = saldoXCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }
}

