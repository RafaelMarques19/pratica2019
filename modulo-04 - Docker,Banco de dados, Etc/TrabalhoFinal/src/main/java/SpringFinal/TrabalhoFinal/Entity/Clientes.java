package SpringFinal.TrabalhoFinal.Entity;


import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "clientes" )
@SequenceGenerator( allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ" )
public class Clientes extends AbstractEntity {

    @Id
    @Column( name = "id_cliente")
    @GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "nome", nullable = false )
    private String nome;

    @Column( name = "cpf", unique = true, nullable = false )
    @CPF
    private String cpf;

    @Column( name = "data_nascimento", nullable = false )
    private LocalDateTime dataNascimento;

    @OneToMany(mappedBy = "cliente")
    private List<Contatos> contatos = new ArrayList<>();

    @OneToMany( mappedBy = "pacote", cascade = CascadeType.ALL )
    private List<ClientesXPacotes> clienteXPacote = new ArrayList<>();

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL )
    private List<Contratacao> contratacoes = new ArrayList<>();

    @OneToMany(mappedBy = "id.cliente", cascade = CascadeType.ALL)
    private List<SaldoXCliente> saldosClientes = new ArrayList<>();

    // GETTERS AND SETTERS

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDateTime getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDateTime dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<Contatos> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contatos> contatos) {
        this.contatos = contatos;
    }

    public List<ClientesXPacotes> getClienteXPacote() {
        return clienteXPacote;
    }

    public void setClienteXPacote(List<ClientesXPacotes> clienteXPacote) {
        this.clienteXPacote = clienteXPacote;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<SaldoXCliente> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoXCliente> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }
}
