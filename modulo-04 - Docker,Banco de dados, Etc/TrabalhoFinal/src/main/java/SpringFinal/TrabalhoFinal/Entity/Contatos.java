package SpringFinal.TrabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import javax.persistence.*;

@Entity
@Table( name = "contatos" )
@SequenceGenerator( allocationSize = 1, name = "CONTATOS_SEQ", sequenceName = "CONTATOS_SEQ" )
public class Contatos extends AbstractEntity {

    @Id
    @Column( name = "id_contato" )
    @GeneratedValue( generator = "CONTATOS_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "fk_id_tipo_contato")
    private TiposContatos tipoContato;

    @Column( name = "valor" )
    private String valor;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_cliente")
    private Clientes cliente;

    // GETTERS AND SETTERS

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public TiposContatos getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TiposContatos tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }
}
