package SpringFinal.TrabalhoFinal.Entity;

import SpringFinal.TrabalhoFinal.Entity.AbstractEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "tipos_contatos" )
@SequenceGenerator( allocationSize = 1, name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ" )
public class TiposContatos extends AbstractEntity{

    @Id
    @Column( name = "id_tipo_contato" )
    @GeneratedValue( generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( name = "nome" )
    private String nome;

    @OneToMany( mappedBy = "tipoContato", cascade = CascadeType.ALL )
    private List<Contatos> contatos = new ArrayList<>();

    // GETTERS AND SETTERS

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Contatos> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contatos> contatos) {
        this.contatos = contatos;
    }
}
