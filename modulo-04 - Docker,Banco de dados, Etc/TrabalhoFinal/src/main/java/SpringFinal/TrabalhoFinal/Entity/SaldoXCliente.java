package SpringFinal.TrabalhoFinal.Entity;


import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "saldos_x_clientes" )
public class SaldoXCliente {

    @Column( name = "id_espacos" )
    @EmbeddedId
    private SaldoXClienteID id;

    @Column(name = "tipo_contratacao", nullable = false)
    @Enumerated( EnumType.STRING )
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private int quantidade;

    @Column(nullable = false)
    private LocalDateTime vencimento;

    @OneToMany(mappedBy = "saldoXCliente")
    private List<Acessos> acessos = new ArrayList<>();

    // GETTERS AND SETTERS

    public SaldoXClienteID getId() {
        return id;
    }

    public void setId(SaldoXClienteID id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDateTime getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDateTime vencimento) {
        this.vencimento = vencimento;
    }

    public void addAcesso(Acessos acesso) {
        this.acessos.add(acesso);
    }

    public void removeAcesso(Acessos acesso) {
        this.acessos.remove(acesso);
    }

    public List<Acessos> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<Acessos> acessos) {
        this.acessos = acessos;
    }
}
