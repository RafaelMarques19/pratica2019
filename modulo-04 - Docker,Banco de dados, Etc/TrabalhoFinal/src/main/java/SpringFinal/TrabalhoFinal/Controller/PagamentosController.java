package SpringFinal.TrabalhoFinal.Controller;


import SpringFinal.TrabalhoFinal.Entity.Pagamentos;
import SpringFinal.TrabalhoFinal.Repository.PagamentosRepository;
import SpringFinal.TrabalhoFinal.Service.PagamentosService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping( value = "/api/pagamento")
public class PagamentosController extends AbstractController<Pagamentos, PagamentosRepository, PagamentosService> {

}