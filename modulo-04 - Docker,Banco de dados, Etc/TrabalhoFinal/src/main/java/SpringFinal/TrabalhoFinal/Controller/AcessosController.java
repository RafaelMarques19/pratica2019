package SpringFinal.TrabalhoFinal.Controller;


import SpringFinal.TrabalhoFinal.Entity.Acessos;
import SpringFinal.TrabalhoFinal.Repository.AcessosRepository;
import SpringFinal.TrabalhoFinal.Service.AcessosService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping( value = "/api/acesso" )
public class AcessosController extends AbstractController<Acessos, AcessosRepository, AcessosService>{


}
