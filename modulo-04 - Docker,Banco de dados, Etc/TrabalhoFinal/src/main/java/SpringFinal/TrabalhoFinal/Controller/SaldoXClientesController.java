package SpringFinal.TrabalhoFinal.Controller;

import SpringFinal.TrabalhoFinal.Entity.SaldoXCliente;
import SpringFinal.TrabalhoFinal.Service.SaldoXClienteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/saldo-cliente" )
public class SaldoXClientesController {

    @Autowired
    SaldoXClienteService saldoXClienteService;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<SaldoXCliente> listarTodos(){
        return saldoXClienteService.todosClientesXSaldos();
    }

    @GetMapping(value = "/{idCliente}-{idEspaco}")
    @ResponseBody
    public SaldoXCliente buscarPorId(@PathVariable Integer idCliente, @PathVariable Integer idEspaco){
        return saldoXClienteService.buscarPorId(idCliente, idEspaco);
    }

    @PostMapping(value = "/adicionar")
    @ResponseBody
    public SaldoXCliente salvar(@RequestBody SaldoXCliente sc){
        return saldoXClienteService.salvar(sc);
    }

    @PutMapping(value = "/editar/{idCliente}-{idEspaco}")
    @ResponseBody
    public SaldoXCliente editar(@PathVariable Integer idCliente, @PathVariable Integer idEspaco, @RequestBody SaldoXCliente sc){
        return saldoXClienteService.editar(idCliente,idEspaco,sc);
    }

    @DeleteMapping(value = "/remover/{idCliente}-{idEspaco}")
    @ResponseBody
    public boolean deletar(@PathVariable Integer idCliente, @PathVariable Integer idEspaco){
        return saldoXClienteService.deletarPorId(idCliente, idEspaco);
    }
}
