package SpringFinal.TrabalhoFinal.Controller;

import SpringFinal.TrabalhoFinal.Entity.Contratacao;
import SpringFinal.TrabalhoFinal.Service.ContratacaoService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/contratacao")
public class ContratacaoController {

    @Autowired
    protected ContratacaoService service;


    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Contratacao> listarTodos(){
        return service.listarTodos();
    }

    @GetMapping(value = "/buscar/{param}")
    @ResponseBody
    public Contratacao buscar(@PathVariable String param) {
        Integer id = Integer.parseInt(param);
        return service.buscarPorId(id);
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public String salvar(@RequestBody Contratacao contratacao) throws Exception{
        return service.salvarString(contratacao);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contratacao editar(@PathVariable Integer id, @RequestBody Contratacao contratacao) throws Exception{
        return service.editar(id, contratacao);
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public boolean remover(@PathVariable Integer id) throws Exception{
        return service.deletarPorId(id);
    }
}
