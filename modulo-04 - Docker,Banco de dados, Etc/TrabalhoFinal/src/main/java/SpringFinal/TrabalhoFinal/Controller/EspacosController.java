package SpringFinal.TrabalhoFinal.Controller;


import SpringFinal.TrabalhoFinal.Entity.Espacos;
import SpringFinal.TrabalhoFinal.Repository.EspacosRepository;
import SpringFinal.TrabalhoFinal.Service.EspacosService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping( value = "/api/espaco")
public class EspacosController extends AbstractController<Espacos, EspacosRepository, EspacosService> {

    @Override
    @GetMapping(value = "/{param}")
    @ResponseBody
    public Espacos buscar(@PathVariable String param) {
        try{
            Integer id = Integer.parseInt(param);
            return entidadeService.buscarPorId(id);
        } catch (NumberFormatException erro){
            return entidadeService.buscarPorNome(param);
        }
    }
}
