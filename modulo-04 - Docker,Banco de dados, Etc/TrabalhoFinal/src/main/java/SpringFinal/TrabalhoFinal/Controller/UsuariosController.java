package SpringFinal.TrabalhoFinal.Controller;


import SpringFinal.TrabalhoFinal.Entity.Usuarios;
import SpringFinal.TrabalhoFinal.Repository.UsuariosRepository;
import SpringFinal.TrabalhoFinal.Service.UsuariosService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping( value = "/api/usuario")
public class UsuariosController extends AbstractController<Usuarios, UsuariosRepository, UsuariosService> {

    @Override
    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public boolean remover(@PathVariable Integer id) throws Exception{
        if(id != 1)
            return super.remover(id);
        else
            return false;
    }

    @Override
    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Usuarios editar(@PathVariable Integer id, @RequestBody Usuarios entidade) throws Exception{
        if(id == 1)
            return null;
        return entidadeService.editar(id, entidade);
    }
}

