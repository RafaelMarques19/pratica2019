package SpringFinal.TrabalhoFinal.Controller;


import SpringFinal.TrabalhoFinal.Entity.ClientesXPacotes;
import SpringFinal.TrabalhoFinal.Repository.ClientesXPacotesRepository;
import SpringFinal.TrabalhoFinal.Service.ClientesXPacotesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/cliente-pacote")
public class ClientesXpacotesController extends AbstractController<ClientesXPacotes, ClientesXPacotesRepository, ClientesXPacotesService> {
}