package SpringFinal.TrabalhoFinal.Controller;


import SpringFinal.TrabalhoFinal.Entity.Espacos;
import SpringFinal.TrabalhoFinal.Entity.EspacosXPacotes;
import SpringFinal.TrabalhoFinal.Repository.EspacosXPacotesRepository;
import SpringFinal.TrabalhoFinal.Service.EspacosXPacotesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping( value = "/api/espaco-pacote")
public class EspacosXPacotesController extends AbstractController<EspacosXPacotes, EspacosXPacotesRepository, EspacosXPacotesService> {

}

