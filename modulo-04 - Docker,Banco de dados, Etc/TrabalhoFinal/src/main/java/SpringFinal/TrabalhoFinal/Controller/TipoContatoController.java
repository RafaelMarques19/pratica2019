package SpringFinal.TrabalhoFinal.Controller;



import SpringFinal.TrabalhoFinal.Entity.TiposContatos;
import SpringFinal.TrabalhoFinal.Repository.TiposContatosRepository;
import SpringFinal.TrabalhoFinal.Service.TiposContatosService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping( value = "/api/tipo-contato")
public class TipoContatoController extends AbstractController<TiposContatos, TiposContatosRepository, TiposContatosService> {

}
