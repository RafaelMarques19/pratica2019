package SpringFinal.TrabalhoFinal.Controller;



import SpringFinal.TrabalhoFinal.Entity.Contatos;
import SpringFinal.TrabalhoFinal.Repository.ContatosRepository;
import SpringFinal.TrabalhoFinal.Service.ContatosService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/contato")
public class ContatoController extends AbstractController<Contatos, ContatosRepository, ContatosService> {
}
