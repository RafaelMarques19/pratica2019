package SpringFinal.TrabalhoFinal.Controller;
import SpringFinal.TrabalhoFinal.Entity.AbstractEntity;
import SpringFinal.TrabalhoFinal.Service.AbstractService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public class AbstractController <
        Entidade extends AbstractEntity,
        EntidadeRepository extends CrudRepository<Entidade, Integer>,
        EntidadeService extends AbstractService<Entidade, EntidadeRepository>> {

    @Autowired
    protected EntidadeService entidadeService;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Entidade> listarTodos(){
        return entidadeService.listarTodos();
    }

    @GetMapping(value = "/{param}")
    @ResponseBody
    public Entidade buscar(@PathVariable String param) {
        Integer id = Integer.parseInt(param);
        return entidadeService.buscarPorId(id);
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public Entidade salvar(@RequestBody Entidade entidade) throws Exception{
        return entidadeService.salvar(entidade);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Entidade editar(@PathVariable Integer id, @RequestBody Entidade entidade) throws Exception{
        return entidadeService.editar(id, entidade);
    }

    @DeleteMapping(value = "/deletar/{id}")
    @ResponseBody
    public boolean remover(@PathVariable Integer id) throws Exception{
        return entidadeService.deletarPorId(id);
    }
}