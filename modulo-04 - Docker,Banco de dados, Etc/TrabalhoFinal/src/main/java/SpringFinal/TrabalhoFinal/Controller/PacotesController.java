package SpringFinal.TrabalhoFinal.Controller;



import SpringFinal.TrabalhoFinal.Entity.Pacotes;
import SpringFinal.TrabalhoFinal.Repository.PacotesRepository;
import SpringFinal.TrabalhoFinal.Service.PacotesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping( value = "/api/pacote")
public class PacotesController extends AbstractController<Pacotes, PacotesRepository, PacotesService>{

}