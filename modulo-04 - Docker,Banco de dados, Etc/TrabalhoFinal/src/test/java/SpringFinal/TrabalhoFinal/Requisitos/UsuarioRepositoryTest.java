package SpringFinal.TrabalhoFinal.Requisitos;


import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertEquals;

import java.util.List;

import SpringFinal.TrabalhoFinal.Entity.Usuarios;
import SpringFinal.TrabalhoFinal.Repository.UsuariosRepository;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuarioRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UsuariosRepository repository;

    @Test
    public void searchByName(){
        Usuarios user = new Usuarios();
        user.setNome("Dbc");
        entityManager.persist(user);
        entityManager.flush();

        Usuarios resposta = repository.findByUsername(user.getNome());

        assertThat(resposta.getNome()).isEqualTo(user.getNome());
    }

    @Test
    public void searchByID(){
        Usuarios user = new Usuarios();
        user.setNome("Dbc");
        user.setId(12);
        entityManager.persist(user);
        entityManager.flush();

        Usuarios resposta = repository.findById(user.getId()).get();

        assertThat(resposta.getId()).isEqualTo(user.getId());
    }

    @Test
    public void searchByEmail(){
        Usuarios user = new Usuarios();
        user.setNome("Dbc");
        user.setEmail("dbccompany@dbccompany.com.br");
        entityManager.persist(user);
        entityManager.flush();

        Usuarios resposta = repository.findByUsername(user.getEmail());

        assertThat(resposta.getEmail()).isEqualTo(user.getEmail());
    }

    @Test
    public void searchByNameList(){
        Usuarios user1 = new Usuarios();
        user1.setUsername("Dbc");
        Usuarios user2 = new Usuarios();
        user2.setEmail("Dbc");

        entityManager.persist(user1);
        entityManager.flush();

        List<Usuarios> resposta = repository.findAllByNome(user1.getNome());

        assertEquals(user1.getNome(), resposta.get(0));
    }

}