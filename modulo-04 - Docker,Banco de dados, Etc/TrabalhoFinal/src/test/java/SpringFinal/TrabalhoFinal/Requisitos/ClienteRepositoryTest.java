package SpringFinal.TrabalhoFinal.Requisitos;

import SpringFinal.TrabalhoFinal.Entity.Clientes;
import SpringFinal.TrabalhoFinal.Entity.Contatos;
import SpringFinal.TrabalhoFinal.Entity.TiposContatos;
import SpringFinal.TrabalhoFinal.Repository.ClientesRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ClienteRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ClientesRepository clienteRepository;

    @Test
    public void findClientByCpf() {
        Clientes cliente = new Clientes();
        cliente.setNome("Andre");
        cliente.setCpf("01592647073");
        cliente.setDataNascimento(LocalDateTime.now());
        entityManager.persist(cliente);
        entityManager.flush();

        Clientes resposta = clienteRepository.findByCpf(cliente.getCpf());
        assertThat(resposta.getCpf()).isEqualTo(cliente.getCpf());
    }

    @Test
    public void testarSalvarClienteComDoisContatosENaoGerarExcecao(){

        Clientes cliente = new Clientes();
        cliente.setNome("Rafael");
        cliente.setCpf("01592647073");
        cliente.setDataNascimento(LocalDateTime.now());

        TiposContatos tipoContato1 = new TiposContatos();
        tipoContato1.setNome("email");
        TiposContatos tipoContato2 = new TiposContatos();
        tipoContato2.setNome("telefone");

        Contatos contato1 = new Contatos();
        contato1.setTipoContato(tipoContato1);
        contato1.setValor("rafael@DbcCompany.com");
        contato1.setCliente(cliente);

        Contatos contato2 = new Contatos();
        contato2.setTipoContato(tipoContato2);
        contato2.setValor("92238475");
        contato2.setCliente(cliente);
        List<Contatos> contatos = new ArrayList<>();
        contatos.add(contato1);
        contatos.add(contato2);
        cliente.setContatos(contatos);

        Clientes resultado = entityManager.persist(cliente);
        entityManager.flush();
        assertThat(clienteRepository.findByCpf("01592647073").getNome()).isEqualTo(cliente.getNome());

    }

    @Test
    public void testarSalvarClienteComDoisContatosEGerarExcecao(){

        Clientes cliente = new Clientes();
        cliente.setNome("Rafael");
        cliente.setCpf("01592647073");
        cliente.setDataNascimento(LocalDateTime.now());

        TiposContatos tipoContato1 = new TiposContatos();
        tipoContato1.setNome("email");
        TiposContatos tipoContato2 = new TiposContatos();
        tipoContato2.setNome("fax");

        Contatos contato1 = new Contatos();
        contato1.setTipoContato(tipoContato1);
        contato1.setValor("rafael@Dbccompany.com");
        contato1.setCliente(cliente);

        Contatos contato2 = new Contatos();
        contato2.setTipoContato(tipoContato2);
        contato2.setValor("92238475");
        contato2.setCliente(cliente);
        List<Contatos> contatos = new ArrayList<>();
        contatos.add(contato1);
        contatos.add(contato2);
        cliente.setContatos(contatos);
        entityManager.persist(cliente);
        entityManager.flush();
        //assertThat().isEqualTo(new Exception());

    }
}
