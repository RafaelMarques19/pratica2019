package SpringFinal.TrabalhoFinal.Requisitos;

import SpringFinal.TrabalhoFinal.Entity.*;
import SpringFinal.TrabalhoFinal.Service.*;
import SpringFinal.TrabalhoFinal.TrabalhoFinalApplicationTests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)

public class PagamentoTest extends TrabalhoFinalApplicationTests {


    @Autowired
    private ClientesService clienteService;
    @Autowired
    private ClientesXPacotesService clientesXPacotesService;
    @Autowired
    private PacotesService pacoteService;
    @Autowired
    private ContratacaoService contratacaoService;
    @Autowired
    private EspacosService espacoService;
    @Autowired
    private SaldoXClienteService saldoXClienteService;
    @Autowired
    private PagamentosService pagamentoService;
    @Autowired
    private EspacosXPacotesService espacoPacoteService;


    @Autowired
    ApplicationContext context;

    @Transactional
    @Test
    public void testarGerarPagamentoGera1SaldoCliente() throws Exception {
        Clientes cliente = new Clientes();
        cliente.setNome("rafael");cliente.setCpf("01592647073");
        TiposContatos tipoContato1 = new TiposContatos();
        tipoContato1.setNome("email");
        TiposContatos tipoContato2 = new TiposContatos();
        tipoContato2.setNome("telefone");
        Contatos contato1 = new Contatos();
        contato1.setTipoContato(tipoContato1);
        contato1.setValor("dani@dani");
        contato1.setCliente(cliente);
        Contatos contato2 = new Contatos();
        contato2.setTipoContato(tipoContato2);
        contato2.setValor("984864572");contato2.setCliente(cliente);
        List<Contatos> contatos = new ArrayList<>();
        contatos.add(contato1);
        contatos.add(contato2);
        cliente.setContatos(contatos);
        try { clienteService.salvar(cliente); } catch (Exception e) { }

        Espacos espaco = new Espacos();espaco.setNome("a"); espaco.setValorReais("R$ 100"); espaco.setQuantidadePessoas(10);
        espacoService.salvar(espaco);

        Contratacao contratacao = new Contratacao();
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setPrazo(30);
        contratacao.setTipoContratacao(TipoContratacao.MES);
        contratacao.setDesconto(20.0);
        contratacao.setQuantidade(5);
        contratacaoService.salvar(contratacao);

        Pagamentos pagamento = new Pagamentos();
        pagamento.setContratacao(contratacao);
        pagamento.setTipoPagamento(TipoPagamento.DEBITO);

        int quantidadeAntes = saldoXClienteService.todosClientesXSaldos().size();
        pagamentoService.salvar(pagamento);
        int quantidadeDepois = saldoXClienteService.todosClientesXSaldos().size();
        assertThat(quantidadeAntes).isEqualTo(0);
        assertThat(quantidadeDepois).isEqualTo(1);
    }

    @Test
    public void testarGerarPagamentoCom2EspacosDeveGerar2SaldoCliente() throws Exception {
        Clientes cliente = new Clientes();
        cliente.setNome("rafael");cliente.setCpf("01592647073");
        TiposContatos tipoContato1 = new TiposContatos();
        tipoContato1.setNome("email");
        TiposContatos tipoContato2 = new TiposContatos();
        tipoContato2.setNome("telefone");
        Contatos contato1 = new Contatos();
        contato1.setTipoContato(tipoContato1);
        contato1.setValor("rafa@rafa");
        contato1.setCliente(cliente);
        Contatos contato2 = new Contatos();
        contato2.setTipoContato(tipoContato2);
        contato2.setValor("984864572");
        contato2.setCliente(cliente);
        List<Contatos> contatos = new ArrayList<>();contatos.add(contato1);contatos.add(contato2);
        cliente.setContatos(contatos);
        try { clienteService.salvar(cliente); } catch (Exception e) { }

        Espacos espaco = new Espacos();
        espaco.setNome("a");
        espaco.setValorReais("R$ 100");
        espaco.setQuantidadePessoas(10);
        espacoService.salvar(espaco);
        Espacos espaco2 = new Espacos();
        espaco2.setNome("b");
        espaco2.setValorReais("R$ 100");
        espaco2.setQuantidadePessoas(10);
        espacoService.salvar(espaco2);

        Pacotes pacote = new Pacotes();
        pacote.setValorReais("R$ 100");
        pacoteService.salvar(pacote);
        ClientesXPacotes clientePacote = new ClientesXPacotes();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(10);
        clientesXPacotesService.salvar(clientePacote);

        EspacosXPacotes espacoPacote = new EspacosXPacotes();
        espacoPacote.setEspaco(espaco);
        espacoPacote.setPacote(pacote);
        espacoPacote.setPrazo(30);
        espacoPacote.setQuantidade(10);
        espacoPacote.setTipoContratacao(TipoContratacao.MES);
        EspacosXPacotes espacoPacote2 = new EspacosXPacotes();
        espacoPacote2.setEspaco(espaco2);espacoPacote2.setPacote(pacote);
        espacoPacote2.setPrazo(30);
        espacoPacote2.setQuantidade(10);espacoPacote2.setTipoContratacao(TipoContratacao.MES);
        espacoPacoteService.salvar(espacoPacote);
        espacoPacoteService.salvar(espacoPacote2);

        Pagamentos pagamento = new Pagamentos();pagamento.setClienteXPacote(clientePacote);
        pagamento.setTipoPagamento(TipoPagamento.DEBITO);

        int quantidadeAntes = saldoXClienteService.todosClientesXSaldos().size();
        pagamentoService.salvar(pagamento);
        int quantidadeDepois = saldoXClienteService.todosClientesXSaldos().size();
        assertThat(quantidadeAntes).isEqualTo(0);
        assertThat(quantidadeDepois).isEqualTo(2);
    }

}
