package SpringFinal.TrabalhoFinal.Requisitos;



import SpringFinal.TrabalhoFinal.Entity.*;
import SpringFinal.TrabalhoFinal.Repository.SaldoXClienteRepository;
import SpringFinal.TrabalhoFinal.Service.AcessosService;
import SpringFinal.TrabalhoFinal.Service.ClientesService;
import SpringFinal.TrabalhoFinal.Service.EspacosService;
import SpringFinal.TrabalhoFinal.TrabalhoFinalApplicationTests;
import javafx.application.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)

@ContextConfiguration(classes= Application.class)
public class AcessoTest extends TrabalhoFinalApplicationTests {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private AcessosService acessosService;
    @Autowired
    private ClientesService clienteService;
    @Autowired
    private EspacosService espacoService;
    @Autowired
    private SaldoXClienteRepository saldoClienteRepository;

    @Before

    public void setUp() {
        Clientes cliente = new Clientes();
        cliente.setNome("rafael");cliente.setCpf("01592647073");
        TiposContatos tipoContato1 = new TiposContatos();
        tipoContato1.setNome("email");
        TiposContatos tipoContato2 = new TiposContatos();
        tipoContato2.setNome("telefone");
        Contatos contato1 = new Contatos();
        contato1.setTipoContato(tipoContato1);
        contato1.setValor("andre@dbccompany.com");
        contato1.setCliente(cliente);
        Contatos contato2 = new Contatos();
        contato2.setTipoContato(tipoContato2);
        contato2.setValor("928365838");
        contato2.setCliente(cliente);
        List<Contatos> contatos = new ArrayList<>();
        contatos.add(contato1);
        contatos.add(contato2);
        cliente.setContatos(contatos);

        try {
            clienteService.salvar(cliente);
        } catch (Exception e) { }

        Espacos espaco = new Espacos();
        espaco.setNome("a");
        espaco.setValorReais("R$ 100");
        espaco.setQuantidadePessoas(10);
        try {
            espacoService.salvar(espaco);
        } catch (Exception e) { }

        SaldoXClienteID saldoId = new SaldoXClienteID();
        saldoId.setCliente(cliente);
        saldoId.setEspaco(espaco);

        SaldoXCliente saldoCliente = new SaldoXCliente();
        saldoCliente.setId(saldoId);
        saldoCliente.setVencimento(LocalDateTime.now().plusDays(60));
        saldoCliente.setTipoContratacao(TipoContratacao.MES);
        saldoCliente.setQuantidade(10);
        Mockito.when(saldoClienteRepository.findById(saldoId).get()).thenReturn(saldoCliente);
    }
    @Transactional
    @Test
    public void testaSeNaoSetandoUmaDataEhAtribuidaDataHoraAtual(){
        Acessos acesso = new Acessos();
        SaldoXCliente encontrado = saldoClienteRepository.findByQuantidade(10);
        acesso.setSaldoXCliente(encontrado);
        acessosService.acessar(acesso);
        LocalDateTime data = acesso.getData();
        assertThat(data).isNotNull();
        assertThat(LocalDateTime.now()).isEqualToIgnoringSeconds(data);
    }

    @Transactional
    @Test
    public void testaSeSaldoVencidoRetornaMensagemDeSaldoInvalido(){
        Acessos acesso = new Acessos();
        SaldoXCliente encontrado = saldoClienteRepository.findByQuantidade(10);
        encontrado.setVencimento(LocalDateTime.now().minusDays(10));
        acesso.setSaldoXCliente(encontrado);
        String retorno = acessosService.acessar(acesso);
        assertThat("O saldo esta vencido").isEqualTo(retorno);
    }

    @Transactional
    @Test
    public void testaRetornoDeSaldoValido(){
        Acessos acesso = new Acessos();
        SaldoXCliente encontrado = saldoClienteRepository.findByQuantidade(10);
        acesso.setSaldoXCliente(encontrado);
        String retorno = acessosService.acessar(acesso);
        assertThat("Saldo:" + acesso.getSaldoXCliente().getVencimento()).isEqualTo(retorno);
    }

    @Transactional
    @Test
    public void testaRetornoDeNaoTerSaldo(){

        try {
            Acessos acesso = new Acessos();
            SaldoXCliente encontrado = saldoClienteRepository.findByQuantidade(10);
            encontrado.setQuantidade(0);
            acesso.setSaldoXCliente(encontrado);
            String retorno = acessosService.acessar(acesso);
            assertThat("Saldo Insuficiente").isEqualTo(retorno);
        } catch (Exception e) { }
    }

    @Transactional
    @Test
    public void testaAtribuicaoDeIsEntradaTrue(){
        Acessos acesso = new Acessos();
        SaldoXCliente encontrado = saldoClienteRepository.findByQuantidade(10);
        acesso.setSaldoXCliente(encontrado);
        acessosService.acessar(acesso);
        boolean retorno = acesso.isEntrada();
        assertThat(retorno).isTrue();
    }
}

