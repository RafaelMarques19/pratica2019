package SpringFinal.TrabalhoFinal.Requisitos;

import SpringFinal.TrabalhoFinal.Controller.ClientesController;
import SpringFinal.TrabalhoFinal.TrabalhoFinalApplicationTests;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class ClienteIntegrationTest extends TrabalhoFinalApplicationTests {


    private MockMvc mock;

    @Autowired
    private ClientesController clientesController;

    @Before
    public void setUp() {
        this.mock = MockMvcBuilders.standaloneSetup(clientesController).build();
    }

    @Test
    public void testGetApiStatusOk() throws  Exception {
        this.mock.perform(MockMvcRequestBuilders.get("/api/cliente")).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
