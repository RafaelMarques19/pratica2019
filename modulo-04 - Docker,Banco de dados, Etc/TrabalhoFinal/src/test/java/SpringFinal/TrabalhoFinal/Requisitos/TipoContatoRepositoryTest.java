package SpringFinal.TrabalhoFinal.Requisitos;

import static org.assertj.core.api.Assertions.*;


import SpringFinal.TrabalhoFinal.Entity.TiposContatos;
import SpringFinal.TrabalhoFinal.Repository.TiposContatosRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TipoContatoRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TiposContatosRepository repository;

    @Test
    public void searchByName(){
        TiposContatos user = new TiposContatos();
        user.setNome("Dbc");
        entityManager.persist(user);
        entityManager.flush();

        TiposContatos resposta = repository.findByNome(user.getNome());

        assertThat(resposta.getNome()).isEqualTo(user.getNome());
    }
}