package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@SequenceGenerator(allocationSize = 1, name = "BANCOS_SEQ", sequenceName = "BANCOS_SEQ")
@Table(name = "BANCOS")
public class Bancos extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "BANCOS_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_BANCO")
	private Integer id;
	@Column(name = "CODIGO")
	private Integer codigo;
	@Column(name = "NOME")
	private String nome;
	@Column(name = "EMPRESTIMOS")
	private double emprestimos;
	
	@OneToMany(mappedBy = "banco", cascade = CascadeType.ALL)
	private List<Agencias> agencias = new ArrayList<>();

	/*
	public Integer getIdBanco() {
		return idBanco;
	}
	
	public void setIdBanco(Integer idBanco) {
		this.idBanco = idBanco;
	}
	*/
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	
 
	public double getEmprestimos() {
		return emprestimos;
	}

	public void setEmprestimos(double emprestimos) {
		this.emprestimos = emprestimos;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}
	@Override
	public void setId(Integer id) {
		// TODO Auto-generated method stub
		this.id = id;
	}
	
	
	
	
}
