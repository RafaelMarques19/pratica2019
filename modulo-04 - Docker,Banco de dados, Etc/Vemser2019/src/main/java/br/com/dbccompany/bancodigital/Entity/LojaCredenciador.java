package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class LojaCredenciador {
	
	//ID COMPOSTO
	@EmbeddedId
	private LojaCredenciadorID id;
	
	private String nome;

	public LojaCredenciadorID getId() {
		return id;
	}

	public void setId(LojaCredenciadorID id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
}
