package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.TipoConta;

public class CorrentistasDTO {
	
	private Integer idCorrentista;
	private String razaoSocial;
	private Integer cnpj;
	private double saldo;
	private TipoConta tipoconta;
	
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public Integer getIdCorrentista() {
		return idCorrentista;
	}
	public void setIdCorrentista(Integer idCorrentista) {
		this.idCorrentista = idCorrentista;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public Integer getCnpj() {
		return cnpj;
	}
	public void setCnpj(Integer cnpj) {
		this.cnpj = cnpj;
	}
	public TipoConta getTipoconta() {
		return tipoconta;
	}
	public void setTipoconta(TipoConta tipoconta) {
		this.tipoconta = tipoconta;
	}
	
	
	
public double Transfere(CorrentistasDTO corre, double valorTransf ) {
		if (valorTransf > this.saldo) {
			System.out.println("Valor transferido � maior que o possuido");
		}else {
			this.saldo -= valorTransf;
			corre.setSaldo(corre.getSaldo() + valorTransf);
		}
		return valorTransf;
		
	}
	
	
}
