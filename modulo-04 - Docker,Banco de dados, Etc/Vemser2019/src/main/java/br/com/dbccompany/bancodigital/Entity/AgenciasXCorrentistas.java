package br.com.dbccompany.bancodigital.Entity;

import java.util.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "AGENCIAS_X_CORRENTISTAS_SEQ", sequenceName = "AGENCIAS_X_CORRENTISTAS_SEQ")

public class AgenciasXCorrentistas {
	@Id
	@GeneratedValue(generator = "AGENCIAS_X_CORRENTISTAS_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer IdAgenciasXCorrentistas;

	public Integer getIdAgenciasXCorrentistas() {
		return IdAgenciasXCorrentistas;
	}

	public void setIdAgenciasXCorrentistas(Integer idAgenciasXCorrentistas) {
		IdAgenciasXCorrentistas = idAgenciasXCorrentistas;
	}
}
	
	