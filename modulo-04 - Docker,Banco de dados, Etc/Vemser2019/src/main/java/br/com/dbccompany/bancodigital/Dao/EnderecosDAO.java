package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class EnderecosDAO extends AbstractDAO<Enderecos> {
	private static final BairrosDAO BAIRROS_DAO = new BairrosDAO();
	
	public Enderecos parseFrom(EnderecosDTO dto) {
		Enderecos enderecos = new Enderecos();
		if(dto.getIdEndereco() != null) {
			enderecos = buscar(dto.getIdEndereco());
		}
		enderecos.setNumero(dto.getNumero());
		enderecos.setComplemento(dto.getComplemento());
		enderecos.setLogradouro(dto.getLogradouro());
		enderecos.setBairro(BAIRROS_DAO.parseFrom(dto.getBairros()));
		
		return enderecos;
		
	}
	
	@Override
	protected Class<Enderecos> getEntityClass(){
		return Enderecos.class;
	}

}
