package br.com.dbccompany.bancodigital.Dto;

public class BancosDTO {
	private Integer idBanco;
	private Integer codigo;
	private String nomes;
	private double emprestimos;
		
	public double getEmprestimos() {
		return emprestimos;
	}
	
	public void setEmprestimos(double emprestimos) {
		this.emprestimos = emprestimos;
	}
	
	public Integer getIdBanco() {
		return idBanco;
	}
	
	public void setIdBanco(Integer idBanco) {
		this.idBanco = idBanco;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public String getNomes() {
		return nomes;
	}
	
	public void setNomes(String nomes) {
		this.nomes = nomes;
	}
	
}
