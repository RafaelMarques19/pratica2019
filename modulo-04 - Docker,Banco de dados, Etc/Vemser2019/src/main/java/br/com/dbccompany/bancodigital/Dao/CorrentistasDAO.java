package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.Correntistas;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class CorrentistasDAO extends AbstractDAO<Correntistas> {
	
	
	public Correntistas parseFrom(CorrentistasDTO dto) {
		Correntistas correntistas = new Correntistas();
		if(dto.getIdCorrentista() != null) {
			correntistas = buscar(dto.getIdCorrentista());
		}
		correntistas.setCnpj(dto.getCnpj());
		correntistas.setRazaoSocial(dto.getRazaoSocial());
		correntistas.setSaldo(dto.getSaldo());
		correntistas.setTipoConta(dto.getTipoconta());	
		return correntistas;
		
	}
	
	
	
	@Override
	protected Class<Correntistas> getEntityClass(){
		return Correntistas.class;
	}

}
