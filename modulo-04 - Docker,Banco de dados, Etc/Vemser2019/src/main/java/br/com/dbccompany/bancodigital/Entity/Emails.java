package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@SequenceGenerator(allocationSize = 1, name = "EMAILS_SEQ", sequenceName = "EMAILS_SEQ")
@Table(name = "EMAILS")


public class Emails extends AbstractEntity {
	
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "EMAILS_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_EMAIL")
	private Integer id;
	@Column(name = "VALOR")
	private String valor;
	
	@ManyToOne
	@JoinColumn(name = "fk_id_cliente")
	private Clientes cliente;
	
	
/*
	public Integer getIdEmail() {
		return idEmail;
	}

	public void setIdEmail(Integer idEmail) {
		this.idEmail = idEmail;
	}
*/
	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		// TODO Auto-generated method stub
		this.id = id;
	}
	
	

}
