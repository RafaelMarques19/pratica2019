package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@SequenceGenerator(allocationSize = 1, name = "BAIRROS_SEQ", sequenceName = "BAIRROS_SEQ")
@Table(name = "BAIRROS")

public class Bairros extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "BAIRROS_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_BAIRRO")
	private Integer id;
	@Column(name = "NOME")
	private String nome;
	
	@ManyToOne
	@JoinColumn(name = "fk_id_cidade")
	private Cidades cidade;
	
	

	public void setCidade(Cidades cidade) {
		this.cidade = cidade;
	}
	@OneToMany(mappedBy = "bairro", cascade = CascadeType.ALL)
	private List<Enderecos> enderecos = new ArrayList<>();
/*
	public Integer getIdBairro() {
		return idBairro;
	}

	public void setIdBairro(Integer idBairro) {
		this.idBairro = idBairro;
	}
	
	
*/
	
	public Cidades getCidade() {
		return cidade;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}
	@Override
	public void setId(Integer id) {
		// TODO Auto-generated method stub
		this.id = id;
	}
	
	
	
	
}
