package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class ClientesDAO  extends AbstractDAO<Clientes>{
	private static final EnderecosDAO ENDERECOS_DAO = new EnderecosDAO();
	
	public Clientes parseFrom(ClientesDTO dto) {
		Clientes clientes = new Clientes();
		if(dto.getIdCliente() != null) {
			clientes = buscar(dto.getIdCliente());
		}
		
		clientes.setNome(dto.getNome());
		clientes.setConjugue(dto.getConjuge());
		clientes.setCpf(dto.getCpf());
		clientes.setRg(dto.getRg());
		clientes.setDataNascimento(dto.getDataNascimento());
		clientes.setEstadoCivil(dto.getEstadoCivil());	
		clientes.setEnderecoCli(ENDERECOS_DAO.parseFrom(dto.getEnderecos()));
		
		return clientes;
		
	}
	
	@Override
	protected Class<Clientes> getEntityClass(){
		return Clientes.class;
	}

}
