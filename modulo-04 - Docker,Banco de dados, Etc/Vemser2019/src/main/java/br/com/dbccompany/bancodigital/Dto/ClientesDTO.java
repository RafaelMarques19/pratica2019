package br.com.dbccompany.bancodigital.Dto;

import br.com.dbccompany.bancodigital.Entity.EstadoCivil;

public class ClientesDTO {
	private Integer  idCliente;
	private String nome;
	private String cpf;
	private String rg;
	private String conjuge;
	private String dataNascimento;
	private EstadoCivil estadoCivil;
	
	private EnderecosDTO enderecos;
	
	public String getNome() {
		return nome;	
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getConjuge() {
		return conjuge;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public void setConjuge(String conjuge) {
		this.conjuge = conjuge;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public EnderecosDTO getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(EnderecosDTO enderecos) {
		this.enderecos = enderecos;
	}
	
	public void depositar(CorrentistasDTO correntista,Double valor) {
		correntista.setSaldo(correntista.getSaldo() + valor);
	}
	
	public void sacar(CorrentistasDTO correntista, Double valor) {
		if (correntista.getSaldo() < valor) {
			System.out.println("n�o � possivel realizar o saque, saldo insuficiente");
		}else {
			correntista.setSaldo(correntista.getSaldo() - valor);
		}
	}
	
	public void pedirEmprestimo(CorrentistasDTO correntista, BancosDTO banco, double valorEmprestado) {
		if (correntista.getSaldo() == 0) {
			correntista.setSaldo(correntista.getSaldo() + valorEmprestado);
			banco.setEmprestimos(banco.getEmprestimos() + valorEmprestado);
		}else {
			System.out.println("Ainda tem saldo na conta, n�o � possivel pedir empr�stimo");
		}
		
	}
	
	
}
