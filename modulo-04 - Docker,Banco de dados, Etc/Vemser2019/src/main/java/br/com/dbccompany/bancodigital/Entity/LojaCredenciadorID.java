package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class LojaCredenciadorID {
	
	@Column(name = "ID_LOJA")
	private Integer idLoja;
	
	@Column(name = "ID_CREDENCIADOR")
	private Integer idCredenciador;
	
	public LojaCredenciadorID() {
		
	}
	
	public LojaCredenciadorID(Integer idLoja, Integer idCredenciador) {
		this.idLoja = idLoja;
		this.idCredenciador = idCredenciador;
	}
	
	
	
}
