package br.com.dbccompany.bancodigital.Entity;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@SequenceGenerator( allocationSize = 1, name = "PAISES_SEQ", sequenceName = "PAISES_SEQ" )
@Table(name = "PAISES")
public class Paises extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue ( generator = "PAISES_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_PAIS")
	private Integer id;
	@Column(name = "NOME")
	private String nome;
	
	@OneToMany( mappedBy = "pais", cascade = CascadeType.ALL)
	private List<Estados> estados = new ArrayList<>();
	
	/*
	public Integer getIdPais() {
		return idPais;
	}
	
	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	} */
	
	public String getNome() {
		return nome;
	}
	
	public List<Estados> getEstados() {
		return estados;
	}

	public void setEstados(List<Estados> estados) {
		this.estados = estados;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		// TODO Auto-generated method stub
		this.id = id;
	}
	
	

}
