package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@SequenceGenerator(allocationSize = 1,name = "CORRENTISTAS_SEQ", sequenceName = "CORRENTISTAS_SEQ")
@Table(name ="CORRENTISTAS")

public class Correntistas extends AbstractEntity {
	/**
	 * 
	 */
	@Id
	@GeneratedValue(generator = "CORRENTISTAS_SEQ", strategy =  GenerationType.SEQUENCE)
	@Column(name = "ID_CORRENTISTA")
	private Integer id;
	@Column(name = "RAZAO_SOCIAL")
	private String razaoSocial;
	@Column(name = "CNPJ")
	private Integer cnpj;
	
	@Column(name = "Saldo")
	private double saldo;
	
	

	@Column(name = "TIPO_CONTA")
	private TipoConta tipoConta;
	
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable( name = "CORRENTISTAS_X_CLIENTES",
				joinColumns = {@JoinColumn(name = "id_correntistas")},
				inverseJoinColumns = {@JoinColumn(name = "id_clientes")})
	private List<Clientes> clientes = new ArrayList<>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "AGENCIAS_X_CORRENTISTAS",
			   joinColumns = {@JoinColumn(name = "id_agencias")},
				inverseJoinColumns = {@JoinColumn (name = "id_correntistas")})
	private List<Agencias> agencias = new ArrayList<>();
	
	/*
	public Integer getIdCorrentista() {
		return idCorrentista;
	}
	
	public void setIdCorrentista(Integer idCorrentista) {
		this.idCorrentista = idCorrentista;
	}
	*/
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	
	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	public Integer getCnpj() {
		return cnpj;
	}
	
	public void setCnpj(Integer cnpj) {
		this.cnpj = cnpj;
	}
	
	public TipoConta getTipoConta() {
		return tipoConta;
	}
	
	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		// TODO Auto-generated method stub
		this.id = id;
	}
}
