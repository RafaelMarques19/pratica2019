package br.com.dbccompany.bancodigital.Entity;

import java.util.List;
import java.util.ArrayList;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name = "TELEFONES_BANCO")
@SequenceGenerator(allocationSize = 1, name= "TELEFONES_SEQ", sequenceName = "TELEFONES_SEQ")

public class Telefones extends AbstractEntity {
	@Id
	@GeneratedValue(generator = "TELEFONES_SEQ", strategy = GenerationType.SEQUENCE)
	@Column( name = "ID_TELEFONE")
	private Integer id;
	
	@Column(name = "NUMERO", length = 100, nullable = false)
	private String numero;
	
	@Enumerated(EnumType.STRING)
	private TipoTelefone tipotelefone;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable( name = "TELEFONES_X_CLIENTES",
				joinColumns = {@JoinColumn(name = "id_telefone")},
				inverseJoinColumns = {@JoinColumn(name = "id_cliente")})
	private List<Clientes> clientes = new ArrayList<>();
	/*
	public Integer getIdTelefone() {
		return idTelefone;
	}
	
	public void setIdTelefone(Integer idTelefone) {
		this.idTelefone = idTelefone;
	}
	*/
	
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	
	public TipoTelefone getTipotelefone() {
		return tipotelefone;
	}
	
	public void setTipotelefone(TipoTelefone tipotelefone) {
		this.tipotelefone = tipotelefone;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		// TODO Auto-generated method stub
		this.id = id;
	}
}
