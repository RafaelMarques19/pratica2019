package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class AgenciasDAO extends AbstractDAO<Agencias>{
	
	private static final EnderecosDAO ENDERECOS_DAO = new EnderecosDAO();
	private static final BancosDAO BANCOS_DAO = new BancosDAO();
	
	public Agencias parseFrom(AgenciasDTO dto) {
		Agencias agencias = new Agencias();
		if(dto.getIdAgencia() != null) {
			agencias = buscar(dto.getIdAgencia());
		}
		agencias.setCodigo(dto.getCodigo());
		agencias.setNome(dto.getNome());
		agencias.setBanco(BANCOS_DAO.parseFrom(dto.getBancos()));
		agencias.setEndereco(ENDERECOS_DAO.parseFrom(dto.getEnderecos()));
	
		
		
		return agencias;
		
	}
	
	@Override
	protected Class<Agencias> getEntityClass(){
		return Agencias.class;
	}

}
