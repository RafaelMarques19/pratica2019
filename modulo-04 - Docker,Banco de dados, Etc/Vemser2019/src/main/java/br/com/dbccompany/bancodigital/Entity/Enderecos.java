package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@SequenceGenerator(allocationSize = 1, name = "ENDERECOS_SEQ",sequenceName = "ENDERECOS_SEQ")
@Table(name = "ENDERECOS")


public class Enderecos extends AbstractEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "ENDERECOS_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_ENDERECO")
	private Integer id;
	@Column(name = "LOGRADOURO")
	private String logradouro;
	@Column(name = "NUMERO")
	private Integer numero;
	@Column(name = "COMPLEMENTO")
	private String complemento;
	
	
	@ManyToOne
	@JoinColumn(name = "fk_id_bairro")
	private Bairros bairro;
	
	@OneToMany(mappedBy = "endereco", cascade = CascadeType.ALL)
	private List<Agencias> agencias = new ArrayList<>();
	@OneToMany(mappedBy = "enderecoCli", cascade = CascadeType.ALL)
	private List<Clientes> cliente = new ArrayList<>();

	/*
	public Integer getIdEndereco() {
		return idEndereco;
	}


	public void setIdEndereco(Integer idEndereco) {
		idEndereco = idEndereco;
	}
*/

	
	public String getLogradouro() {
		return logradouro;
	}


	public Bairros getBairro() {
		return bairro;
	}


	public void setBairro(Bairros bairro) {
		this.bairro = bairro;
	}


	public List<Agencias> getAgencias() {
		return agencias;
	}


	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	}


	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}


	public Integer getNumero() {
		return numero;
	}


	public void setNumero(Integer numero) {
		this.numero = numero;
	}


	public String getComplemento() {
		return complemento;
	}


	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}


	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		// TODO Auto-generated method stub
		this.id = id;
	}
	
	
	

}
