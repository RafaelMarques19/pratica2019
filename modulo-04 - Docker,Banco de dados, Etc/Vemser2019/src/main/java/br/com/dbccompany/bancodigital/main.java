package br.com.dbccompany.bancodigital;

import java.util.logging.Logger;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Entity.EstadoCivil;
import br.com.dbccompany.bancodigital.Entity.TipoConta;
import br.com.dbccompany.bancodigital.Service.AgenciasService;
import br.com.dbccompany.bancodigital.Service.BairrosService;
import br.com.dbccompany.bancodigital.Service.BancosService;
import br.com.dbccompany.bancodigital.Service.CidadesService;
import br.com.dbccompany.bancodigital.Service.ClientesService;
import br.com.dbccompany.bancodigital.Service.CorrentistasService;
import br.com.dbccompany.bancodigital.Service.EnderecosService;
import br.com.dbccompany.bancodigital.Service.EstadosService;
import br.com.dbccompany.bancodigital.Service.PaisesService;

public class main {
	
	private static final Logger LOG = Logger.getLogger(main.class.getName());

	public static void main(String[] args) {
		//PAISES
		PaisesService service = new PaisesService();
		PaisesDTO paises =  new PaisesDTO();
		paises.setNome("PaisTeste");
		service.salvarPaises(paises);
			
		PaisesDTO paises2 =  new PaisesDTO();
		paises2.setNome("PaisTeste2");
		service.salvarPaises(paises2); 
		//ESTADOS		
		EstadosService serviceEs = new EstadosService();
		EstadosDTO estados = new EstadosDTO();
		estados.setNome("estadoTeste1");
		estados.setPaises(paises);
		serviceEs.salvarEstados(estados);
			
		EstadosDTO estados2 = new EstadosDTO();
		estados2.setNome("estadoTeste2");
		estados2.setPaises(paises2);
		serviceEs.salvarEstados(estados2);
		
		//CIDADES
		CidadesService serviceCi = new CidadesService();
		CidadesDTO cidades = new CidadesDTO();
		cidades.setNome("CidadeTeste");
		cidades.setEstados(estados);
		serviceCi.salvarCidades(cidades);
		
		
		CidadesDTO cidades2 = new CidadesDTO();
		cidades2.setNome("CidadeTeste2");
		cidades2.setEstados(estados2);
		serviceCi.salvarCidades(cidades2);
		
		BairrosService serviceBa = new BairrosService();
		BairrosDTO bairros = new BairrosDTO();
		bairros.setNome("BairroTeste");
		bairros.setCidades(cidades);
		serviceBa.salvarBairros(bairros);
		
		BairrosDTO bairros2 = new BairrosDTO();
		bairros2.setNome("BairroTeste");
		bairros2.setCidades(cidades2);
		serviceBa.salvarBairros(bairros2);
		
		//Enderecos
		EnderecosService serviceEn = new EnderecosService();
		EnderecosDTO enderecos = new EnderecosDTO();
		enderecos.setComplemento("complemento1");
		enderecos.setLogradouro("logradouroTeste");
		enderecos.setNumero(1);
		enderecos.setBairros(bairros);
		serviceEn.salvarEnderecos(enderecos);
		
		EnderecosDTO enderecos2 = new EnderecosDTO();
		enderecos2.setComplemento("complemento2");
		enderecos2.setLogradouro("logradouroTeste2");
		enderecos2.setNumero(2);
		enderecos2.setBairros(bairros2);
		serviceEn.salvarEnderecos(enderecos2);
		
		//Bancos
		BancosService serviceBan = new BancosService();
		BancosDTO bancos = new BancosDTO();
		bancos.setCodigo(1);
		bancos.setNomes("bancoTeste");
		bancos.setEmprestimos(0);
		serviceBan.salvarBancos(bancos);
		
		BancosDTO bancos2 = new BancosDTO();
		bancos2.setCodigo(2);
		bancos2.setNomes("bancoTeste2");
		serviceBan.salvarBancos(bancos2);
		
		//Agencias
		AgenciasService serviceAg = new AgenciasService();
		AgenciasDTO agencias = new AgenciasDTO();
		agencias.setBancos(bancos);
		agencias.setEnderecos(enderecos);
		agencias.setCodigo(1);
		agencias.setNome("AgTeste1");
		serviceAg.salvarAgencias(agencias);
		
		AgenciasDTO agencias2 = new AgenciasDTO();
		agencias2.setBancos(bancos2);
		agencias2.setEnderecos(enderecos2);
		agencias2.setCodigo(2);
		agencias2.setNome("AgTeste2");
		serviceAg.salvarAgencias(agencias2);
		
		//Clientes
		ClientesService serviceCli = new ClientesService();
		ClientesDTO clientes = new ClientesDTO();
		clientes.setNome("ClienteTeste1");
		clientes.setConjuge("uniao_estavel");
		clientes.setCpf("123.456.789-12");
		clientes.setRg("0000000");
		clientes.setDataNascimento("1/1/2001");
		clientes.setEstadoCivil(EstadoCivil.SOLTEIRO);
		clientes.setEnderecos(enderecos);
		serviceCli.salvarClientes(clientes);
		
		ClientesDTO clientes2 = new ClientesDTO();
		
		clientes2.setNome("Cliente1");
		clientes2.setConjuge("uniao_estavel");
		clientes2.setCpf("987.654.321-98");
		clientes2.setRg("00000001");
		clientes2.setDataNascimento("1/1/2002");
		clientes2.setEstadoCivil(EstadoCivil.SOLTEIRO);
		clientes2.setEnderecos(enderecos2);
		serviceCli.salvarClientes(clientes2);
		
		
		//Correntistas
		CorrentistasService serviceCor = new CorrentistasService();
		CorrentistasDTO correntistas = new CorrentistasDTO();
		correntistas.setCnpj(123456);
		correntistas.setRazaoSocial("Sim");
		correntistas.setTipoconta(TipoConta.PF);
		correntistas.setSaldo(100.0);
		serviceCor.salvarCorrentistas(correntistas);
		
		CorrentistasDTO correntistas2 = new CorrentistasDTO();
		correntistas2.setCnpj(123456);
		correntistas2.setRazaoSocial("Nao");
		correntistas2.setTipoconta(TipoConta.PJ);
		correntistas2.setSaldo(100.0);
		serviceCor.salvarCorrentistas(correntistas2);
		
		correntistas.Transfere(correntistas2, 100.0);
		System.out.println(correntistas2.getSaldo());
		System.out.println(correntistas.getSaldo());
		
		serviceCor.salvarCorrentistas(correntistas);
		serviceCor.salvarCorrentistas(correntistas2);
		
		clientes.pedirEmprestimo(correntistas, bancos, 500);
		serviceCor.salvarCorrentistas(correntistas);
		serviceBan.salvarBancos(bancos);
		
		
		
		
		
		
		
	/*	BancosService bancoss = new BancosService();
		BancosDTO bancos = new BancosDTO();
		bancos.setCodigo(1001);
		bancos.setNomes("bradesco");
		bancoss.salvarBancos(bancos); */
		
		/*	Paises paises = new Paises();
		paises.setNome("Brasil");
		service.salvarPaises(paises); */
		
	}
	/*public static void main(String[] args) {
		
		Session session = null;	
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSession();
			transaction = session.beginTransaction();
			
			Paises paises = new Paises();
			paises.setNome("Brasil");
			
			session.save(paises);
			
			//session.createQuery("select * from paises;").executeUpdate();
			
			Criteria criteria = session.createCriteria(Paises.class);
			criteria.createAlias("nome", "nome_paises");
			criteria.add(
					Restrictions.isNotNull("nome")
					);
			
			List<Paises> lstPaises = criteria.list();
			
			
			transaction.commit();
		}catch(Exception e) {
			if(transaction != null) {
				transaction.rollback();
			}
			LOG.log(Level.SEVERE,e.getMessage(),e);
			System.exit(1);
		} finally {
			if(session != null) {
				session.close();;
			}
		}
		System.exit(0);
	}
*/
}
