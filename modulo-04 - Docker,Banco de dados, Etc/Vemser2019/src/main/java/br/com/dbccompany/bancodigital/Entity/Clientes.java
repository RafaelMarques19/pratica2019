package br.com.dbccompany.bancodigital.Entity;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CLIENTES")
@SequenceGenerator(allocationSize = 1, name = "CLIENTES_SEQ",sequenceName = "CLIENTES_SEQ")
public class Clientes extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_CLIENTE", length = 100, nullable = false)
	private Integer id;
	
	@Column(name = "NOME")
	private String nome;
	
	@Column(name = "CPF", length = 100, nullable = false)
	private String cpf;
	
	
	@Column(name = "RG", length = 100, nullable = false)
	private String rg;
	
	@Column(name = "CONJUGUE", length = 100, nullable = false)
	private String conjugue;
	
	

	
	@Column(name = "DATA_NASCIMENTO", length = 100, nullable = false)
	private String dataNascimento;
	
	@Enumerated(EnumType.STRING)
	private EstadoCivil estadoCivil;
	
	@ManyToMany(mappedBy = "clientes")
	private List<Telefones> telefones = new ArrayList<>();
	
	@ManyToMany(mappedBy = "clientes")
	private List<Correntistas> correntistas = new ArrayList<>();
	
	@ManyToOne
	@JoinColumn(name = "fk_id_endereco")
	private Enderecos enderecoCli;
	
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
	private List<Emails> emails = new ArrayList<>();

	
	
	/*
	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	} */



	
	
	
	public String getNome() {
		return nome;
	}

	

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getConjugue() {
		return conjugue;
	}

	public void setConjugue(String conjugue) {
		this.conjugue = conjugue;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}
	@Override
	public void setId(Integer id) {
		// TODO Auto-generated method stub
		this.id = id;
	}



	public Enderecos getEnderecoCli() {
		return enderecoCli;
	}



	public void setEnderecoCli(Enderecos enderecoCli) {
		this.enderecoCli = enderecoCli;
	}



	




	
	
	
	
	
	

}
