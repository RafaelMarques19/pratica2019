package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Estados;

public class BancosDAO extends AbstractDAO<Bancos> {
	
	
	public Bancos parseFrom(BancosDTO dto) {
		Bancos bancos = new Bancos();
		if(dto.getIdBanco() != null) {
			bancos = buscar(dto.getIdBanco());
		}else {
			
		}
		bancos.setCodigo(dto.getCodigo());
		bancos.setNome(dto.getNomes());
		bancos.setEmprestimos(dto.getEmprestimos());

	
		return bancos;		
	}
	
	@Override
	protected Class<Bancos> getEntityClass(){
		return Bancos.class;
	}

}
