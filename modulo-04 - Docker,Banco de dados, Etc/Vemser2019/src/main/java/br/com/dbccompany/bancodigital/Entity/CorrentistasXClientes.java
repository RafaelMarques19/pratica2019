package br.com.dbccompany.bancodigital.Entity;

import java.util.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "CORRENTISTAS_X_CLIENTES_SEQ", sequenceName = "CORRENTISTAS_X_CLIENTES_SEQ")



public class CorrentistasXClientes {
	@Id
	@GeneratedValue(generator = "CORRENTISTAS_X_CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer IdCorrentistasXClientes;

	public Integer getIdCorrentistasXClientes() {
		return IdCorrentistasXClientes;
	}

	public void setIdCorrentistasXClientes(Integer idCorrentistasXClientes) {
		IdCorrentistasXClientes = idCorrentistasXClientes;
	}

	

	
	
	
	
}
