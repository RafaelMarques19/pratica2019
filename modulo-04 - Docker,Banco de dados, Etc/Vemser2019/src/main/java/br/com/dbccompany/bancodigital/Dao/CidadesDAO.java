package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Estados;



public class CidadesDAO extends AbstractDAO<Cidades> {
	
	private static final EstadosDAO ESTADOS_DAO = new EstadosDAO();
 
	public Cidades parseFrom(CidadesDTO dto) {
		Cidades cidades = new Cidades();
		if(dto.getIdCidade() != null) {
			cidades = buscar(dto.getIdCidade());
		}
		cidades.setId(dto.getIdCidade());
		cidades.setNome(dto.getNome());
		cidades.setEstado(ESTADOS_DAO.parseFrom(dto.getEstados()));
		return cidades;
		
	}
	
	@Override
	protected Class<Cidades> getEntityClass(){
		return Cidades.class;
	}

}
