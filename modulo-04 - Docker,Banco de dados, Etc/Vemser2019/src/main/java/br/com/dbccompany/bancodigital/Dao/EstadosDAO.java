package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.Entity.Paises;
;



public class EstadosDAO extends AbstractDAO<Estados> {
	
	private static final PaisesDAO PAISES_DAO = new PaisesDAO();
	

	public Estados parseFrom(EstadosDTO dto) {
		Estados estados = new Estados();
		if(dto.getIdEstados() != null) {
			estados = buscar(dto.getIdEstados());
		}else {
		}
		estados.setId(dto.getIdEstados());
		estados.setNome(dto.getNome());
		 estados.setPais(PAISES_DAO.parseFrom(dto.getPaises()));
		return estados;
		
	}
	
	@Override
	protected Class<Estados> getEntityClass(){
		return Estados.class;
	}
	
	
	
	

}
