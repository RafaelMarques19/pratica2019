package br.com.dbccompany.bancodigital.Entity;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;


@Entity
@SequenceGenerator(allocationSize = 1, name= "ESTADOS_SEQ",sequenceName = "ESTADOS_SEQ")

public class Estados extends AbstractEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue( generator=  "ESTADOS_SEQ",strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_ESTADO")
	private Integer id;
	@Column(name = "NOME")
	private String nome;
	
	//@OneToOne MAPEAR DOS DOIS LADOS
	//@OneToMany
	//@ManyToOne
	//@ManyToMany
	
	@ManyToOne
	@JoinColumn(name = "fk_id_pais")
	private Paises pais;
	//@OneToMany( mappedBy = "pais", cascade = CascadeType.ALL)

	
	@OneToMany (mappedBy = "estado", cascade = CascadeType.ALL)
	private List<Cidades> cidades = new ArrayList<>();
	/*
	public Integer getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}
*/
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Paises getPais() {
		return pais;
	}

	public void setPais(Paises pais) {
		this.pais = pais;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		// TODO Auto-generated method stub
		this.id = id;
	}
	
}
