import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Login from './pages/Login'
import {PrivateRoute} from  './components/PrivateRoute';
import PaginaInicial from './pages/PaginaInicial'
import ListaElfos from "./pages/ListaElfos"
import ListaDwarfs from "./pages/ListaDwarfs"
import CadastraElfo from "./pages/CadastraElfo"

import './App.css';
export default class App extends Component {
  constructor ( props ) { // eslint-disable-line no-useless-constructor
    super( props )
    this.props = props
  }
  render(){
    return (
      <Router>
        <React.Fragment>
          <Route path="/login" exact component={ Login } />
          <PrivateRoute path="/" exact component={ PaginaInicial } />
          <PrivateRoute  path="/api/elfo/" exact component={ListaElfos}></PrivateRoute>
          <PrivateRoute path= "/api/dwarf/Dwarf" exact component={ListaDwarfs}></PrivateRoute>
          <PrivateRoute path="/api/elfo/novo" exact component={CadastraElfo}></PrivateRoute>

        </React.Fragment>
      </Router>
    );
  }
}

