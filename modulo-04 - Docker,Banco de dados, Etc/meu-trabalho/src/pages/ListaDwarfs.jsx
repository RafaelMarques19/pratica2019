import React, {Component} from 'react';
import Cabecalho from '../components/Cabecalho/Cabecalho'
import "./../components/ListaElfos/ListaElfos.css"
import * as axios from "axios"

export default class ListaDwarfs extends Component{
    constructor(props){
        super(props)
        this.state = {
            ListaDwarfs : [],
            
        }        
    }
    componentDidMount() {
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:8080/api/dwarf/Dwarf', header).then(resp=>
            
            {   console.log(resp)
                const Dwarf = resp.data;
                this.setState({
                    ListaDwarfs : Dwarf,
                    
                })
            })
    }

    render() {
        const {ListaDwarfs} = this.state
        return (
            <React.Fragment>
                <Cabecalho nomeC={"Dwarfs"}/>
            { ListaDwarfs.map((Dwarf, index) => { 
                return(
                    <React.Fragment key={index}>
                        <div className="container">
                            <div className="Dwarf">
                                    <h5 >ID: {Dwarf.id}</h5>
                                    <h5 >Nome: {Dwarf.nome}</h5>
                                    <h5 >Experiencia: {Dwarf.experiencia} </h5>
                                    <h5 >Vida: {Dwarf.vida} </h5>
                                    <h5 >Dano: {Dwarf.dano} </h5>
                            </div>
                        </div>
                    </React.Fragment> 
                )
            }) }
            </React.Fragment>
        )   
    }
}