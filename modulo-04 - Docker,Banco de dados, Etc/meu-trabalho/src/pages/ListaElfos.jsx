import React, {Component} from 'react';
import Cabecalho from '../components/Cabecalho/Cabecalho'
import './../components/ListaElfos/ListaElfos.css'
import * as axios from 'axios';



export default class ListaElfos extends Component{
    constructor(props){
        super(props)
        this.state = {
            ListaElfos : [],
            ListaAlterada: []
        }        
    }
    componentDidMount() {
        const header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        axios.get('http://localhost:8080/api/elfo/', header).then(resp=>
            
            {   console.log(resp)
                const elfos = resp.data;
                this.setState({
                    ListaElfos : elfos,
                    
                })
            })
    }

    render() {
        const {ListaElfos} = this.state
        return (
            <React.Fragment>
               <Cabecalho nomeC={"Elfos"}/>
            { ListaElfos.map((elfos, index) => { 
                return(
                    <React.Fragment key={index}>
                        <div className="container">
                            <div className="elfos">
                                    <h5>ID: {elfos.id}</h5>
                                    <h5>Nome: {elfos.nome}</h5>
                                    <h5>Experiencia: {elfos.experiencia} </h5>
                                    <h5>Vida: {elfos.vida} </h5>
                                    <h5>Dano: {elfos.dano} </h5>
                            </div>
                        </div>
                    </React.Fragment> 
                )
            }) }
        </React.Fragment>
        )   
    }
}


{/* <p key={agencias.codigo} id={agencias.id}>{agencias.codigo}{agencias.nome} {agencias.id} </p> */}