import React, {Component} from 'react';
import ContainerHome from './../components/ContainerHome/ContainerHome'
import Cabecalho from './../components/Cabecalho/Cabecalho'

import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

export default class PaginaInicial extends Component{
    render(){
        return(
            <React.Fragment>
                <Cabecalho nomeC={'Pagina Inicial'}/>
                
                     <Link to='/api/elfo/'>
                        <ContainerHome nome={'Lista Elfos'} caminho={'/api/elfo/'}/>
                    </Link>
               
                    <Link to='/api/dwarf/Dwarf'>
                        <ContainerHome nome={'Lista Dwarfs'} caminho={'/api/dwarf/Dwarf'}/>
                    </Link>
                        
                    <Link to='/api/elfo/novo'>
                        <ContainerHome nome={'Cadastra Elfos'} caminho={'/api/elfo/novo'}/>
                    </Link>
            </React.Fragment>
        )
    }
}