import React, { Component } from 'react';
import * as axios from 'axios';
import './login.css'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
        this.trocaValoresState = this.trocaValoresState.bind( this )
    }
    trocaValoresState(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }
    logar(e) {
        e.preventDefault();
        const { username, password } = this.state
        if (username && password) {
            //executo a regra de login
            console.log('username', username);
            console.log('password', password);
            axios.post('http://localhost:8080/login', {
                username: this.state.username,
                password: this.state.password
            }).then( resp => {
                    localStorage.setItem( 'Authorization', resp.headers.authorization );
                  //  localStorage.setItem( 'Username', username );
                    this.props.history.push("/")
                }
            )
        }
    }
    render() {
        return (
            <React.Fragment>
                <header className="cabecalho">
                    <h1>Seja Bem Vindo</h1>
                </header>
                <div className="boxInputs">
                    <input type="text" name="username" id="username" placeholder="Digite o email" onChange={ this.trocaValoresState } />  
                </div>
                <div className="boxInputs">
                    <input type="password" name="password" id="password" placeholder="Digite o password" onChange={ this.trocaValoresState }/>
                </div>
                <div className="boxInputs">
                    <button type="button" onClick={ this.logar.bind( this ) }>Logar</button> 
                </div>          
            </React.Fragment>
        ) 
    }
}