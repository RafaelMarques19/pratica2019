import React, { Component } from 'react'
import * as axios from 'axios';
import Cabecalho from '../components/Cabecalho/Cabecalho';
import './../components/ListaElfos/ListaElfos.css'
import './login.css'

export default class CadastraElfo extends Component{
    constructor(props) {
        super(props)
        this.state = {
            nome: "",
            vida: "",
            experiencia: "",
            dano: ""
        }
        this.trocaValoresState = this.trocaValoresState.bind(this);
        this.newElfo = this.newElfo.bind(this);
    }

    trocaValoresState(e) {
        const {name, value} = e.target
        this.setState({
            [name]: value
        })
    }

    newElfo(e){
        e.preventDefault();
        const { match: { params } } = this.props
        const header = {
            headers: { Authorization: localStorage.getItem('Authorization') }
        }
        const { nome} = this.state
        if (nome) {
            axios.post('http://localhost:8080/api/elfo/novo/',
            {
                nome: this.state.nome,
                vida: this.state.vida,
                experiencia: this.state.experiencia,
                dano: this.state.dano
            }, header).then(resp => {
                this.props.history.push('/')
                console.log(resp)
             })
        }
    }

    render(){
        return(
            <React.Fragment>
                <Cabecalho nomeC={"Cadastro Elfos"}/>   
                <div className="boxInputs">
                    <input  type="text" name="nome"  id="nome"  placeholder="Nome" onChange={this.trocaValoresState}/>
                </div>
                <div className="boxInputs">
                    <input  type="text" name="vida" id="vida" placeholder="Vida" onChange={this.trocaValoresState}/>
                </div>
                <div className="boxInputs">
                    <input  type="text" name="experiencia"  id="experiencia"  placeholder="Experiencia" onChange={this.trocaValoresState}/>
                </div>
                <div className="boxInputs">
                    <input  type="text" name="dano" id="dano" placeholder="Dano" onChange={this.trocaValoresState}/>
                </div>

                <div className="boxInputs">
                    <button type="button" onClick={this.newElfo}>Novo Elfo</button>
                </div>                
             </React.Fragment>
        )
    }


}