import React, {Component} from 'react'
import './ContainerHome.css'

export default class ContainerHome extends Component{
    constructor(props){
        super(props)
        this.props = props;
    }


    render(){
        return(
            <div className="container">
                <a href={this.props.caminho}> 
                    <div>
                        <h3>{this.props.nome}</h3>
                    </div>
                </a>
            </div>
        )
    }
}